class MainMailer < ActionMailer::Base
  default from: "no-reply@okc-russia.ru",
          to: "isaevdenismich@mail.ru"

  def callback_mail(payment)
    @payment = payment
    mail(to: payment.email, subject: 'Бронирование билетов okc-russia.ru')
  end

  def admin_mail(payment)
    @payment = payment
    mail(to: "aidar_kc_rossia@mail.ru", subject: 'Произведено бронирование билетов okc-russia.ru через сайт')
  end

end
