module ApplicationHelper
  def generate_order_button_code ( event_id )
    return "<div class = \"kassir_online_btn\" data-event-id = \"#{event_id}\"
            style = \"
              padding: 5px 10px 5px 10px;
              background-color: blue;
              margin: 20px;
              color: white;
              display: table;
              cursor: pointer;
            \">Забронировать билет</div>"
  end
end
