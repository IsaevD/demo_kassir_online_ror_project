module TicketsHelper

  def generate_new_tickets(event)
    arr = []
    ticket_state_id = TicketState.find_by(:alias => TicketState.AVAILABLE_ALIAS).id
    SeatItem.where(:schema_item_id => SchemaItem.find_by(:hall_id => event.hall.id).id).each do |seat|
      arr << {:seat_item_id => seat.id, :price => seat.seat_item_type.price, :event_item_id => event.id, :ticket_state_id => ticket_state_id }
    end
    Ticket.create(arr)
  end

  def extend_tickets(event)
    arr = []
    ticket_state_id = TicketState.find_by(:alias => TicketState.AVAILABLE_ALIAS).id
    tickets = Ticket.where(:event_item_id => event.id)
    s_s_h = SectorGroup.find_by(:alias => "main_hall_with_seats_hall").id
    s_h = SectorGroup.find_by(:alias => "main_hall_hall").id
    s_s_b = SectorGroup.find_by(:alias => "main_hall_with_seats_balkon").id
    s_b = SectorGroup.find_by(:alias => "main_hall_balkon").id


    SeatItem.where(:schema_item_id => SchemaItem.find_by(:hall_id => Hall.find_by(:alias => "main_hall_with_seats").id).id).each do |seat|
      flag = false
      tickets.each do |ticket|
        old_seat_item = ticket.seat_item
        if ((old_seat_item.sector_group_id == s_h && seat.sector_group_id == s_s_h) || (old_seat_item.sector_group_id == s_b && seat.sector_group_id == s_s_b)) && (old_seat_item.row == seat.row) && (old_seat_item.row == seat.row) && (old_seat_item.number == seat.number) && (old_seat_item.schema_item_id != seat.schema_item_id)
          ticket.seat_item = seat
          flag = true
          ticket.save
          break
        end
      end
      if !flag
        arr << {:seat_item_id => seat.id, :price => seat.seat_item_type.price, :event_item_id => event.id, :ticket_state_id => ticket_state_id }
      end
    end
    Ticket.create(arr)
  end

  def get_schema(event)
    schema = {}
    Seat.where(:hall_id => event.hall_id).select(:seat_group_id).group(:seat_group_id).each do |seat|
      ranges = {}
      Seat.where(:hall_id => event.hall_id).where(:seat_group_id => seat.seat_group_id).order(:range => :asc).select(:range).group(:range).each do |seat_range|
        ranges[seat_range.range] = Seat.where(:hall_id => event.hall_id).where(:seat_group_id => seat.seat_group_id, :range => seat_range.range).order(:position => :asc)
      end
      schema[seat.seat_group_id] = ranges
    end
    return schema
  end

  def get_legend(event)
    seat_items = event.hall.schema_item.seat_items
    seat_items.select(:seat_item_type_id).each do |item|
      item
    end
  end

end