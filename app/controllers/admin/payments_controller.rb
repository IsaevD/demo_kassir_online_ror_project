class Admin::PaymentsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update

    entity = @entity
    id = params[:id]
    path = @path
    is_log = true

    @item = entity.find(id)

    @fields.each do |field, settings|
      entity_params = params[@param_name]
      case settings[:type]
        when :image
          if entity_params[field] == "nil"
            params[@param_name][field] = nil
          end
        when :dual_list
          if settings[:show_in_card]
            current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
            current_values.destroy_all
            values = entity_params[field]
            if !values.nil?
              values.each do |key, value|
                settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
              end
            end
          end
        when :image_list
          if !entity_params[settings[:image_entity_field]].nil?
            entity_params[settings[:image_entity_field]].each do |key, value|
              if value == "nil"
                @fields[field][:image_entity].find(key).destroy
              end
            end
          end
          if !entity_params[field].nil?
            entity_params[field].each do |value|
              image_list = @fields[field][:image_entity].new
              image_list.update_attribute(@fields[field][:entity_field], params[:id])
              image_list.update_attribute(@fields[field][:image_entity_field], value)
              image_list.save
            end
          end
      end
    end

    if @item.update_attributes(prepared_params(@param_name, @params_array))
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
      end
      # Обновление билетов
      tickets = Ticket.where(:payment_id => @item.id)
      tickets.each do |ticket|
        ticket.payment_id = nil
        ticket.ticket_state = TicketState.available_state
        ticket.save
      end
      if !params[@param_name][:ticket_id].nil?
        params[@param_name][:ticket_id].each do |key, value|
          ticket = Ticket.find(key)
          if @item.payment_type.alias == PaymentType.PURCHASE_ALIAS
            ticket.ticket_state = TicketState.bought_state
          else
            if @item.payment_type.alias == PaymentType.RESERVATION_ALIAS
              ticket.ticket_state = TicketState.booked_state
            end
          end
          ticket.payment_id = @item.id
          ticket.save
        end
      end
      redirect_to path
    else
      render "new"
    end
    #update_item(@entity, @path, params[:id], true)
  end

  def destroy
    @item = @entity.find(params[:id])

    # Возвращение статусов билетов
    Ticket.where(:payment_id => @item.id).each do |ticket|
      ticket.payment_id = nil
      ticket.ticket_state = TicketState.available_state
      ticket.save
    end

    @item.destroy
    redirect_to @path

    #delete_item(@entity, @path, params[:id], true)
  end

  private

    def init_variables

      @object = {
          :name => "payments",
          :entity => Payment,
          :param_name => :payment,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.payments_path,
              :new_path => nil,
              :edit_path => Rails.application.routes.url_helpers.payments_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Платежи" => nil
          },
          :fields => {
              :number => {
                  :type => :string,
                  :label => "Номер платежа",
                  :show_in_card => true,
                  :show_in_table => true
              },
              :updated_at => {
                  :type => :datetime,
                  :label => "Дата",
                  :show_in_card => true,
                  :show_in_table => true
              },
              :payment_type_id => {
                  :type => :collection,
                  :label => "Тип",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => PaymentType,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :event_item_id =>  {
                  :type => :collection,
                  :label => "Мероприятие",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => EventItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  }
              },
              :system_user_id =>  {
                  :type => :collection,
                  :label => "Кассир",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemUser,
                  :where_visible_field => :login,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  },
                  :help_message => "Кассир, который производил покупку/бронирование билета через систему"
              },
              :delivery_label => {
                  :type => :group_label,
                  :label => "Информации о клиенте"
              },
              :fio =>  {
                  :type => :string,
                  :label => "Имя/Фамилия",
                  :show_in_card => true,
                  :show_in_table => false
              },
              :phone =>  {
                  :type => :string,
                  :label => "Телефон",
                  :show_in_card => false,
                  :show_in_table => false
              },
              :email => {
                  :type => :string,
                  :label => "Email",
                  :show_in_table => true,
                  :show_in_card => true
              },
              :ticket_label => {
                  :type => :group_label,
                  :label => "Билеты"
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
