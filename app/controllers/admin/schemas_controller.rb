class Admin::SchemasController < Admin::ApplicationController

  skip_before_filter :signed_in_system?, :only => [:ajax_create_payment_and_book_ticket_by_ids, :ajax_master_create_payment_and_book_ticket_by_ids]

  def index
    # toDO: рефакторинг
    @event_item = EventItem.find(params[:event_id])
    case @event_item.hall.alias
      when "main_hall"
        @hall = SectorGroup.find_by(:alias => "main_hall_hall")
        @balkon = SectorGroup.find_by(:alias => "main_hall_balkon")
      when "main_hall_with_seats"
        @hall = SectorGroup.find_by(:alias => "main_hall_with_seats_hall")
        @balkon = SectorGroup.find_by(:alias => "main_hall_with_seats_balkon")
      when "theatr_hall"
        @hall = SectorGroup.find_by(:alias => "theatr_hall_hall")

    end
  end

  def only_schema
    @event_item = EventItem.find(params[:event_id])

    case @event_item.hall.alias
      when "main_hall"
        @hall = SectorGroup.find_by(:alias => "main_hall_hall")
        @balkon = SectorGroup.find_by(:alias => "main_hall_balkon")
      when "main_hall_with_seats"
        @hall = SectorGroup.find_by(:alias => "main_hall_with_seats_hall")
        @balkon = SectorGroup.find_by(:alias => "main_hall_with_seats_balkon")
    end
  end

  def print_blank
    @ticket = Ticket.find(params[:ticket_id])
    render :layout => "blank"
  end

  def accept_blank_request
    if request.post?
      blank_request = BlankRequest.find(params[:blank_request_id])
      if !blank_request.nil?
        blank_request.blank_delivery_type = BlankDeliveryType.to_kassir_type
        kassir = Kassir.get_kassir_by_user(current_system_user)
        case blank_request.blank_type.alias
          when BlankType.NORMAL_ALIAS
            kassir.normal_counter = kassir.normal_counter + blank_request.value
          when BlankType.SPECIAL_ALIAS
            kassir.special_counter = kassir.special_counter + blank_request.value
        end
        kassir.save
        blank_request.save
      end
      redirect_to main_page_path
    else
      @blank_requests = BlankRequest.where(:system_user_id => current_system_user.id, :blank_delivery_type => BlankDeliveryType.delivery_type.id)
    end
  end

  # Получить платеж и билеты
  def ajax_get_reservation_payment_by_number
    number = params[:number]
    global_arr = []
    payments = []
    if !number.empty?
      payments = Payment.where("payment_type_id = #{PaymentType.reservation_state.id} AND (number = '#{number}' OR phone = '#{number}')")
      if (payments.count != 0)
        payment = payments.first
        payment.tickets.each do |ticket|
          ticket_price = ticket.price
          ticket_discount = "-"
          if !ticket.ticket_discount.nil?
            if payment.updated_at <= ticket.ticket_discount.date
              ticket_price = TicketDiscount.get_price_by_discount_without_date_link(ticket)
              ticket_discount = ticket.ticket_discount.name
            else
              ticket_price = ticket.price
            end
          end
          arr = []
          seat_item = ticket.seat_item
          arr << ticket.id
          arr << seat_item.sector_group.name
          arr << seat_item.row
          arr << seat_item.number
          arr << ticket_discount
          arr << ticket_price
          global_arr << arr
        end
        render :json => [payment.id, payment.event_item.name, global_arr]
      else
        render :json => []
      end
    else
      render :json => []
    end
  end

  # Купить билеты
  def ajax_buy_ticket_by_ids
    ids = params[:ticket_ids]
    payment_id = params[:payment_id]
    payment = Payment.find(payment_id)
    # Обновление билетов
    Ticket.where(:payment_id => payment_id).each do |ticket|
      ticket.payment_id = nil
      ticket.ticket_state = TicketState.available_state
      ticket.save
    end
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.payment_id = payment.id
      ticket.ticket_state = TicketState.bought_state
      ticket.save
    end
    payment.payment_type = PaymentType.purchase_state
    payment.system_user_id = current_system_user.id
    payment.save
    render :text => "Success"
  end

  # Сформировать платеж и оплатить билеты
  def ajax_create_payment_and_buy_ticket_by_ids
    ids = params[:ticket_ids]
    payment = Payment.create_payment(nil, current_system_user.id, params[:event_id], PaymentType.purchase_state)
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.bought_state
      ticket.payment_id = payment.id
      ticket.save
    end
    render :text => "Success"
  end

  # Сформировать платеж и забронировать билет
  def ajax_create_payment_and_book_ticket_by_ids
    ids = params[:ticket_ids]
    user = current_system_user
    if !user.nil?
      user = user.id
    end
    payment = Payment.create_payment(nil, user, params[:event_id], PaymentType.reservation_state, nil, params[:phone], nil, params[:fio])
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.booked_state
      ticket.payment_id = payment.id
      ticket.save
    end
    render :text => "Success"
  end

  def ajax_master_create_payment_and_book_ticket_by_ids
    event = EventItem.find_by(:id => params[:event_id])
    if !event.nil?
      ids = params[:ticket_ids]
      user = current_system_user
      if !user.nil?
        user = user.id
      end
      tickets = Ticket.where("id in (#{ids})")
      if tickets.count != tickets.where(:ticket_state_id => TicketState.available_state.id).count
        # некоторые или все билеты, поступившие в заказе, уже проданы (не доступны)
        render :json => {
            status: "error" ,
            message: "Произошла ошибка" ,
            payment_num: "" ,
            email: ""}
      else
        payment = Payment.create_payment(nil, user, params[:event_id], PaymentType.reservation_state, nil, nil, nil, params[:fio], params[:email])
        tickets.each do |ticket|
          ticket.ticket_state = TicketState.booked_state
          ticket.payment_id = payment.id
          ticket.save
        end
        if params[:email].nil?
          site_mail_answer = ""
        else
          site_mail_answer = "Бронь зарегистрирована и отправлена на почту: " + params[:email]
        end
        # msg = "Ваш номер брони - #{payment.number.to_s}. Мероприятие: #{event_name}"
        MainMailer.callback_mail(payment).deliver
        MainMailer.admin_mail(payment).deliver
        render :json => {
            status: "success" ,
            message: "Вы успешно забронировали места на сеанс. Выкупить билеты можно по номеру брони",
            payment_num: "Ваш номер брони: " + payment.number.to_s,
            email: site_mail_answer
        }

      end
    end
  end

  # Получить информацию о билетах
  def ajax_usual_get_tickets_info
    tickets = Ticket.get_tickets_by_event_id(
        params[:event_id]).map {|i| [
        i.seat_item_id,
        i.ticket_state.alias,
        TicketDiscount.get_price_by_discount(i),
        i.ticket_state.name,
        SeatItemType.get_color_by_price(i.price),
        Ticket.get_discount_name(i)
    ]}
    render :json => tickets
  end


  def ajax_book_ticket_by_ids
    ids = params[:ticket_ids]
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.booked_state
      ticket.system_user_id = current_system_user.id
      ticket.save
    end
    render :text => "Success"
  end

  def ajax_get_ticket_by_id
    id = params[:ticket_id]
    ticket = Ticket.find(id)
    render :json => [ticket.price]
  end

  def ajax_get_tickets
    tickets = Ticket.get_tickets_by_event_id(params[:event_id]).map {|i| [i.seat_item_id, i.ticket_state.alias, i.price] }
    render :json => tickets
  end

end
