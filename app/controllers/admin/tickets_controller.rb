class Admin::TicketsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

    def init_variables

      @object = {
          :name => "tickets",
          :entity => Ticket,
          :param_name => :ticket,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.tickets_path,
              :new_path => nil,
              :edit_path => Rails.application.routes.url_helpers.tickets_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Билеты" => nil
          },
          :fields => {
              :event_item_id =>  {
                  :type => :collection,
                  :label => "Мероприятие",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => EventItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :ticket_state_id =>  {
                  :type => :collection,
                  :label => "Состояние",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => TicketState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :price => {
                  :type => :integer,
                  :label => "Цена",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :user_id =>  {
                  :type => :collection,
                  :label => "Пользователь сайта",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => User,
                  :where_visible_field => :last_name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  }
              },
              :system_user_id =>  {
                  :type => :collection,
                  :label => "Пользователь системы",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemUser,
                  :where_visible_field => :login,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
