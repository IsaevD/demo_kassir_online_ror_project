class Admin::ReportsController < Admin::ApplicationController
  
  def sales_report
    @data = {}
    @results = {
      :result => {
      },
      :prices => {}
    }
    # Обработка параметров фильтрации
    if params[:filter].nil?
      of = to = ""
    else
      of = params[:filter][:datetime][:date_interval][:of]
      to = params[:filter][:datetime][:date_interval][:to]
    end
    # Не выбрана не одна дата => обрабатываем все мероприятия
    if of.empty? && to.empty?
      payments = Payment.all
    # Выбраны обе даты => щем в казанном интервале
    elsif !of.empty? && !to.empty?
      of = Date.parse(of).beginning_of_day
      to = Date.parse(to).end_of_day
      payments = Payment.where(:updated_at => of..to)    
    # Выбрана граница "от" => обрабатываем все мероприятия, дата ПРОВЕДЕНИЯ которых не меньше указанной
    elsif !of.empty?
      of = Date.parse(of).beginning_of_day
      payments = Payment.where(["updated_at >= ?", of])  
    # Выбрана граница "до" => обрабатываем все мероприятия, дата ПРОВЕДЕНИЯ которых не больше указанной
    elsif !to.empty?
      to = Date.parse(to).end_of_day
      payments = Payment.where(["updated_at <= ?", to])          
    end
    payments.each do |payment|
        event = payment.event_item
        # все купленные билеты, сгруппированные по ценам
        tickets_by_states = payment.tickets.where("ticket_state_id = ? OR ticket_state_id = ?", TicketState.bought_state.id, TicketState.booked_state.id).group_by(&:ticket_state_id)
        if tickets_by_states.count > 0 
          # Массивы для данных по мероиприятиям и общему результату
          if @data[event.name].nil?
            @data[event.name] = {}
            @data[event.name][:data] = {}
          end 

          tickets_by_states.each do |ticket_state_id, value|
            state_alias = TicketState.find(ticket_state_id).alias

            tickets_by_prices = value.group_by(&:price)
            
            tickets_by_prices.each do | price , tickets |
              count = tickets.count
              sold_at =  tickets.count * price.to_i
              # формируем массив данных по мероприятиям
              # для конкретной цены за билет
              if @data[event.name][:data][state_alias].nil?
                @data[event.name][:data][state_alias] = {}
              end
              
              if @data[event.name][:data][state_alias][price].nil? 
                @data[event.name][:data][state_alias][price] = { :count => count, :sold_at => sold_at }
              else 
                @data[event.name][:data][state_alias][price][:count] += count
                @data[event.name][:data][state_alias][price][:sold_at] += sold_at
              end
              #  для всех билетов сразу
              if @data[event.name][:result].nil?
                @data[event.name][:result] = {}
              end
              if @data[event.name][:result][state_alias].nil?
                @data[event.name][:result][state_alias] = {:count => 0 , :sold_at => 0}
              end
              @data[event.name][:result][state_alias][:count] += count
              @data[event.name][:result][state_alias][:sold_at] += sold_at 
              #  Проверяем, есть ли запись по проверяемой цене билета 
              if @results[:prices][state_alias].nil?
                @results[:prices][state_alias] = {}
              end
              if @results[:prices][state_alias][price].nil? 
                @results[:prices][state_alias][price] = { 
                    :sold_at => 0,
                    :count => 0 
                }
              end
              if @results[:result][state_alias].nil?
                @results[:result][state_alias] = {}
                @results[:result][state_alias] = { 
                    :sold_at => 0,
                    :count => 0 
                }
              end              
              # высчитываем результат по всем меропиятиям
              # для конкретной цены за билет
              @results[:prices][state_alias][price][:sold_at] += sold_at 
              @results[:prices][state_alias][price][:count] += count
              #  Для всех билетов сразу
              @results[:result][state_alias][:sold_at] += sold_at
              @results[:result][state_alias][:count] += count
            end
          end
        end
    end
    # сортируем данные по ценам
    if @results[:result].empty? 
      @results[:result]["bought"] = @results[:result]["booked"] = {
        :sold_at => 0,
        :count => 0 
      }
    else 
      @results[:prices].each do | ticket_state , data_by_prices |
        @results[:prices][ticket_state] = data_by_prices.sort
      end
    end
    @data.each do | event , ticket_state |
      @data[event][:data].each do | ticket_state_alias , data |
        @data[event][:data][ticket_state_alias] = @data[event][:data][ticket_state_alias].sort 
      end 
    end
  end
end