class Admin::TicketDiscountsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

    def init_variables

      @object = {
          :name => "ticket_discounts",
          :entity => TicketDiscount,
          :param_name => :ticket_discount,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.ticket_discounts_path,
              :new_path => Rails.application.routes.url_helpers.new_ticket_discount_path,
              :edit_path => Rails.application.routes.url_helpers.ticket_discounts_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Скидки на билеты" => nil
          },
          :fields => {
              :alias => {
                  :type => :string,
                  :label => "Алиас",
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true
              },
              :name => {
                  :type => :string,
                  :label => "Наименование",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :value => {
                  :type => :string,
                  :label => "Значение",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :is_percent => {
                  :type => :checkbox,
                  :label => "В процентах",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => "Если не указано, то скидка будет номинальной"
              },
              :date => {
                  :type => :datetime,
                  :label => "Действует до",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :help_message => "Если дата станет больше текущей, то скидка автоматически отключится"
              },
              :event_item_id =>  {
                  :type => :collection,
                  :label => "Мероприятие",
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => EventItem,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
