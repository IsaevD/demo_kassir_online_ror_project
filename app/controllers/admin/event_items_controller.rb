class Admin::EventItemsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    entity = @entity
    path = @path
    is_log = true
    is_redirect = true

    @item = entity.new(prepared_params(@param_name, @params_array))
    if @item.save
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :dual_list
            if settings[:show_in_card]
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], @item.id)
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end

      generate_new_tickets(@item)

      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
      end
      if (is_redirect)
        redirect_to path
      end
    else
      render "new"
    end
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  def extend
    event_item_id = params[:event_id]
    event = EventItem.find(event_item_id)
    if !event.nil?
      if event.hall.alias == "main_hall"
        extend_tickets(event)
        event.hall = Hall.find_by(:alias => "main_hall_with_seats")
        event.save
        redirect_to "/event_items/#{event_item_id}/edit"
      end
    end
  end

  private

    def init_variables

      @object = {
          :name => "event_items",
          :entity => EventItem,
          :param_name => :event_item,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.event_items_path,
              :new_path => Rails.application.routes.url_helpers.new_event_item_path,
              :edit_path => Rails.application.routes.url_helpers.event_items_path
          },
          :breadcrumbs => {
              t("custom.modules.main_page.name") => "Rails.application.routes.url_helpers.main_page_path",
              "Мероприятия" => nil
          },
          :fields => {
              :id => {
                  :type => :string,
                  :label => "ИД",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :description => {
                  :type => :text,
                  :label => "Описание",
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true
              },
              :content => {
                  :type => :html,
                  :label => t('form.labels.content'),
                  :show_in_card => false,
                  :show_in_table => false,
                  :required => true
              },
              :date => {
                  :type => :datetime,
                  :label => "Дата мероприятия",
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :image => {
                  :type => :image,
                  :label => t('form.labels.image'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :hall_id =>  {
                  :type => :collection,
                  :label => "Площадка",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => Hall,
                  :where_visible_field => :name,
                  :where_statement => {
                      "system_state_id" => SystemMainItemsD::SystemState.active_state.id
                  },
                  :settings => {
                      :include_blank => false
                  }
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
