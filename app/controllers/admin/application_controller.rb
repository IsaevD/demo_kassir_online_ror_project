class Admin::ApplicationController < ActionController::Base
  include DataProcessingD::ApplicationHelper
  include ObjectGeneratorD::ApplicationHelper
  include AuthorizeD::ApplicationHelper
  include SystemUsersAuthorizeD::ApplicationHelper
  include ActionView::Helpers::UrlHelper
  include AccessRightsD::ApplicationHelper
  include SystemUserActionsLogD::ApplicationHelper
  include TicketsHelper
  layout "admin"
  helper ObjectGeneratorD::ApplicationHelper
  helper AuthorizeD::ApplicationHelper
  helper SystemUsersAuthorizeD::ApplicationHelper
  helper AccessRightsD::ApplicationHelper
  helper SystemUserActionsLogD::ApplicationHelper

  before_filter :init_app_variables, :signed_in_system?

  def signed_in_system?
    signed_in_user(system_users_authorize_d.sign_in_path, current_system_user)
  end

  private

    def init_app_variables
      items = params[:controller].split("/")
      @module_name = items[0]
      @module_item_alias = items[1]
      @module_item = ModulesD::ModuleItem.find_by(:module_name => @module_name, :alias => @module_item_alias)
      if signed_in(current_system_user)
        menus = {
            "root" => {
                :main_page => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => "Главная",
                    :value => Rails.application.routes.url_helpers.main_page_path
                },
                :events => {
                    :type => :two_step_menu,
                    :icon => "icon-test",
                    :label => "Управление мероприятиями",
                    :value => {
                        :event_items => {
                            :type => :url,
                            :label => "Мероприятия",
                            :value => Rails.application.routes.url_helpers.event_items_path
                        },
                        :halls => {
                            :type => :url,
                            :label => "Площадки",
                            :value => Rails.application.routes.url_helpers.halls_path
                        }
                    }
                },
                :tickets => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => "Управление билетами",
                  :value => {
                      :tickets => {
                          :type => :url,
                          :label => "Платежи",
                          :value => Rails.application.routes.url_helpers.payments_path
                      },
                      :discounts => {
                          :type => :url,
                          :label => "Скидки",
                          :value => Rails.application.routes.url_helpers.ticket_discounts_path
                      },
                      :group => {
                          :type => :url,
                          :label => "Ценовые группы",
                          :value => Rails.application.routes.url_helpers.seat_item_types_path
                      }
                  }
              },
              :reports => {
                :type  => :two_step_menu,
                :icon  => "icon-test",
                :label => t('labels.menu.reports'),
                :value => {
                  :sales_result => {
                    :type  => :url,
                    :label => t('labels.menu.sales_report'),
                    :value => Rails.application.routes.url_helpers.sales_report_path 
                  },
                },
              },
            },
            "administrator" => {
                :events => {
                    :type => :two_step_menu,
                    :icon => "icon-test",
                    :label => "Управление мероприятиями",
                    :value => {
                        :event_items => {
                            :type => :url,
                            :label => "Мероприятия",
                            :value => Rails.application.routes.url_helpers.event_items_path
                        }
                    }
                },
                :tickets => {
                    :type => :two_step_menu,
                    :icon => "icon-test",
                    :label => "Управление билетами",
                    :value => {
                        :tickets => {
                            :type => :url,
                            :label => "Заявки",
                            :value => Rails.application.routes.url_helpers.payments_path
                        },
                        :discounts => {
                            :type => :url,
                            :label => "Скидки",
                            :value => Rails.application.routes.url_helpers.ticket_discounts_path
                        },
                        :group => {
                            :type => :url,
                            :label => "Ценовые группы",
                            :value => Rails.application.routes.url_helpers.seat_item_types_path
                        }
                    }
                },
                :reports => {
                  :type  => :two_step_menu,
                  :icon  => "icon-test",
                  :label => t('labels.menu.reports'),
                  :value => {
                    :sales_result => {
                      :type  => :url,
                      :label => t('labels.menu.sales_report'),
                      :value => Rails.application.routes.url_helpers.sales_report_path 
                    },
                  },
                },
            },
            "kassir" => {
                :main_page => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => "Главная",
                    :value => Rails.application.routes.url_helpers.main_page_path
                },
                :reports => {
                  :type  => :two_step_menu,
                  :icon  => "icon-test",
                  :label => t('labels.menu.reports'),
                  :value => {
                    :sales_result => {
                      :type  => :url,
                      :label => t('labels.menu.sales_report'),
                      :value => Rails.application.routes.url_helpers.sales_report_path 
                    },
                  },
                },
            },
        }
        @menu_items = menus[current_system_user.system_user_role.alias]
      end

    end


end
