class Admin::IndexController < Admin::ApplicationController

  skip_before_filter :signed_in_system?, :only => [:master, :test, :get_event_data]
  before_filter :allow_iframe_requests

  def index
=begin
    if params[:role_alias].nil?
      @role_alias = "none"
    else
      @role_alias = params[:role_alias]
    end
=end
    if (current_system_user.system_user_role.alias == "administrator")
      redirect_to event_items_path
    end


  end

  def master
    @advanced_mode = false
    @error = nil
    if (params[:event_id].is_a? Integer) || params[:event_id] == "empty"
      @error = "Сеанс не наиден"
    else
      @event_item = EventItem.find_by(:id => params[:event_id])
    end
    if @event_item.nil?
      @error = "Сеанс не наиден"
    else
      case @event_item.hall.alias
        when "main_hall"
          @hall = SectorGroup.find_by(:alias => "main_hall_hall")
          @balkon = SectorGroup.find_by(:alias => "main_hall_balkon")
        when "main_hall_with_seats"
          @hall = SectorGroup.find_by(:alias => "main_hall_with_seats_hall")
          @balkon = SectorGroup.find_by(:alias => "main_hall_with_seats_balkon")
        when "theatr_hall"
          @hall = SectorGroup.find_by(:alias => "theatr_hall_hall")
      end
    end
    render layout: "application"
  end

  def get_event_data
    response.headers["Access-Control-Allow-Origin"] = "*"
    output = {
      :status => "error",
      :data => I18n.t('labels.errors.event_id_is_empty'),
    }
    unless params[:event_id].nil?
      event = EventItem.find_by(:id => params[:event_id].to_i)
      unless event.nil?
        output[:status] = "success"
        output[:data] = {
          :name => event.name,
          :date => event.date.strftime("%d.%m.%Y %H:%M")
        }
      end
    end
    render :json => output
  end

  def test
    render layout: "application"
  end

  private

    def allow_iframe_requests
      response.headers.delete('X-Frame-Options')
    end

end