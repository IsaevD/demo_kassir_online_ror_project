class Admin::AdvancedSchemasController < Admin::ApplicationController

  def index
    @event_item = EventItem.find(params[:event_id])
    # @object = {
    #     :header_label => "#{@event_item.name} - #{@event_item.date.strftime("%d")} #{Rails.application.config.months[@event_item.date.strftime("%B")]} #{@event_item.date.strftime("%H:%M")} - #{@event_item.hall.name}"
    # }
    @advanced_mode = true
    case @event_item.hall.alias
      when "main_hall"
        @hall = SectorGroup.find_by(:alias => "main_hall_hall")
        @balkon = SectorGroup.find_by(:alias => "main_hall_balkon")
      when "main_hall_with_seats"
        @hall = SectorGroup.find_by(:alias => "main_hall_with_seats_hall")
        @balkon = SectorGroup.find_by(:alias => "main_hall_with_seats_balkon")
      when "theatr_hall"
        @hall = SectorGroup.find_by(:alias => "theatr_hall_hall")

    end

  end

  # Покупка билетов
  def ajax_buy_ticket
    ids = params[:ticket_ids]
    event_id = params[:event_id]
    tickets = Ticket.where("id in (#{ids})")
    Ticket.create_purchase_payment(tickets, event_id, current_system_user)
    render :text => "Success"
  end
  # Бронирование билетов
  def ajax_book_ticket
    ids = params[:ticket_ids]
    event_id = params[:event_id]
    tickets = Ticket.where("id in (#{ids})")
    Ticket.create_reservation_payment(tickets, event_id, current_system_user)
    render :text => "Success"
  end
  # Печать билета
  def ajax_print_ticket
    ticket_id = params[:ticket_id]
    ticket = Ticket.find(ticket_id)
    ticket = Ticket.create_ticket_normal_number(ticket)
    if ticket.number.nil?
      BlankDefect.create_blank_defect(ticket, current_system_user)
    end
    ticket.save
    render :text => "Success"
  end


  # Изменение статуса билетов
  def ajax_change_ticket_states
    ids = params[:ticket_ids]
    ticket_state = TicketState.find(params[:ticket_state_id])
    flag = ((ticket_state.alias == TicketState.BOUGHT_ALIAS) || (ticket_state.alias == TicketState.BOOKED_ALIAS))
    if flag
      if (ticket_state.alias == TicketState.BOOKED_ALIAS)
        payment_type = PaymentType.purchase_state
      end
      if (ticket_state.alias == TicketState.BOUGHT_ALIAS)
        payment_type = PaymentType.reservation_state
      end
      payment = Payment.create_payment(nil, current_system_user.id, params[:event_id], payment_type)
    end
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = ticket_state
      if flag
        ticket.payment_id = payment.id
      end
      if (ticket_state.alias == TicketState.BOUGHT_ALIAS)
        if ticket.number.nil?
          counter = Setting.generate_normal_counter_number
          ticket.number = "#{counter} #{Setting.get_setting(Setting.NORMAL_COUNTER_PREFIX_ALIAS)}"
          Setting.set_setting(Setting.NORMAL_COUNTER_NUMBER_ALIAS, counter)
        end
      end
      ticket.save
    end
    render :text => "Success"
  end

  # Изменение цены билетов
  def ajax_change_ticket_prices
    ids = params[:ticket_ids]
    price = params[:price].to_i
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.price = price
      ticket.save
    end
    render :text => "Success"
  end

  # Получить информацию о билетах
  def ajax_get_tickets_info
    tickets = Ticket.get_tickets_by_event_id(
        params[:event_id]).map {|i| [
          i.seat_item_id,
          i.ticket_state.alias,
          TicketDiscount.get_price_by_discount(i),
          i.ticket_state.name,
          SeatItemType.get_color_by_price(i.price),
          Ticket.get_discount_name(i),
          !i.payment.nil? ? i.payment.id : "" ,
          !i.payment.nil? ? i.payment.number : "",
          !i.payment.nil? ? (!i.payment.user.nil? ? i.payment.user.id : "" ) : "",
          !i.payment.nil? ? (!i.payment.user.nil? ? i.payment.user.email : "" ) : "",
          !i.payment.nil? ? i.payment.system_user_id : "",
          !i.payment.nil? ? (!i.payment.system_user_id.nil? ? SystemMainItemsD::SystemUser.find(i.payment.system_user_id).login : "" ) : "",
          0
    ]}
    render :json => tickets
  end

  # Изменение скидки на билет
  def ajax_change_ticket_discounts
    ids = params[:ticket_ids]
    ticket_discount_id = params[:ticket_discount_id].to_i
    if ticket_discount_id == 0
      ticket_discount_id = nil
    end
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_discount_id = ticket_discount_id
      ticket.save
    end
    render :text => "Success"
  end

  def ajax_buy_ticket_by_ids
    ids = params[:ticket_ids]
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.bought_state
      ticket.system_user_id = current_system_user.id
      ticket.save
    end
    Payment.create_payment_with_tickets(Ticket.where("id in (#{ids})"), "cash", nil, nil, nil)
    render :text => "Success"
  end

  def ajax_book_ticket_by_ids
    ids = params[:ticket_ids]
    Ticket.where("id in (#{ids})").each do |ticket|
      ticket.ticket_state = TicketState.booked_state
      ticket.system_user_id = current_system_user.id
      ticket.save
    end
    render :text => "Success"
  end

  def ajax_get_ticket_by_id
    id = params[:ticket_id]
    ticket = Ticket.find(id)
    render :json => [ticket.price]
  end

  def ajax_get_tickets
    tickets = Ticket.get_tickets_by_event_id(params[:event_id]).map {|i| [i.seat_item_id, i.ticket_state.alias, i.price] }
    render :json => tickets
  end

  private

    def init_variables
    end

end
