class Desktop::ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include DataProcessingD::ApplicationHelper
  include ObjectGeneratorD::ApplicationHelper
  include AuthorizeD::ApplicationHelper
  include SystemUsersAuthorizeD::ApplicationHelper
  include ActionView::Helpers::UrlHelper
  include AccessRightsD::ApplicationHelper
  include SystemUserActionsLogD::ApplicationHelper
  include TicketsHelper
  layout "admin_auth"
  helper ObjectGeneratorD::ApplicationHelper
  helper AuthorizeD::ApplicationHelper
  helper SystemUsersAuthorizeD::ApplicationHelper
  helper AccessRightsD::ApplicationHelper
  helper SystemUserActionsLogD::ApplicationHelper

end
