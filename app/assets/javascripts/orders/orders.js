// Отметить все значения в таблице
function checkAllOrderTableValues() {
    $("#order #all").click(function(){
        $("#order table input[type=checkbox]").prop("checked", $(this).prop("checked"));
    });
}

// Очистить записи в таблице заказа
function clearRecordInOrderTable() {
    $("#order .clear_table").click(function(){
        $("#order table input[type=checkbox]").each(function(){
            if ($(this).prop("checked") == true) {
                $(this).parent().parent().parent().remove();
            }
        });
        recalculateOrderPrices();
    });
}
function recalculateOrderPrices() {
    price = 0;
    count = 0;
    $("#order table tr.value").each(function(){
        price = price + parseInt($(this).children(".price").data("price"));
        count = count + parseInt($(this).children(".count").data("count"));
    });
    $("#order table .total_count").text(count+" шт.");
    $("#order table .total_price").text(price+" руб.");
    setHeaderCartWidgetMessage(count, price);
}

// Работа фейкового поля типа Checkbox
function checkActionValue() {
    $(".inputs input[type=checkbox]").click(function(){
        $(".inputs input[type=checkbox]").prop("checked", false);
        $(this).prop("checked", true);
    });
}

// Переинициализировать куки
function submitConfirmStage() {
    $(".confirm_stage #order").submit(function(){
        arrayIds = [];
        $(".confirm_stage #order tr.value").each(function(){
            arrayIds.push($(this).data("ticket-id"));
        });
        setTicketsCookie(arrayIds);
        return true;
    });
}


// Отметить все значения в таблице заказа
function checkAllPaymentTableValues() {
    $("#payment_tickets #all").click(function(){
        $("#payment_tickets table input[type=checkbox]").prop("checked", $(this).prop("checked"));
    });
}

// Очистить записи в таблице заказа
function clearRecordInPaymentTable() {
    $("#payment_tickets .clear_table").click(function(){
        $("#payment_tickets table input[type=checkbox]").each(function(){
            if ($(this).prop("checked") == true) {
                $(this).parent().parent().parent().remove();
            }
        });
        recalculatePaymentPrices();
    });
}
function recalculatePaymentPrices() {
    price = 0;
    count = 0;
    $("#payment_tickets table tr.value").each(function(){
        price = price + parseInt($(this).children(".price").data("price"));
        count = count + parseInt($(this).children(".count").data("count"));
    });
    $("#payment_tickets table .total_count").text(count+" шт.");
    $("#payment_tickets table .total_price").text(price+" руб.");
}