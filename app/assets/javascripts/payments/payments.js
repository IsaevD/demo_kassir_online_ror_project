function initializePaymentSearch() {
    $(".search_field form").submit(function(){
        $("#tickets").hide();
        $.ajax({
            type: "POST",
            data: {"number": $(".search_field #number").val() },
            url: "/ajax_get_reservation_payment_by_number",
            success: function (data) {

                $("#tickets .label").text("");
                $("#tickets .table .ticket").remove();
                $("#tickets .non-record").remove();
                $(".search_result input[type=submit]").hide();

                if (data.length != 0) {
                    $("#tickets").show();
                    $("#tickets").attr("data-payment-id", data[0]);
                    $("#tickets .label").text(data[1]);
                    $(".search_result input[type=submit]").show();
                    var total = 0;
                    data[2].forEach(function (item, i, arr) {
                        total = total + parseInt(item[5]);
                        $("#tickets .table tbody").append("<tr class='ticket'>" +
                            "<td>" +
                            "<input type='hidden' class='ticket_id' value='" + item[0] + "'>" +
                            item[1] +
                            "</td>" +
                            "<td>" + item[2] + "</td>" +
                            "<td>" + item[3] + "</td>" +
                            "<td>" + item[4] + "</td>" +
                            "<td class='price'>" + item[5] + "</td>" +
                            "<td><div class='btn red_btn remove_ticket'>Удалить</div></td>" +
                            "</tr>");
                    });
                    $("#tickets .table tbody").append("<tr class='ticket'>" +
                        "<td colspan='4'>Итого</td>" +
                        "<td class='total'>" + total + "</td>" +
                        "<td></td>" +
                        "</tr>"
                    );
                } else {
                    $("#tickets").show();
                    $("#tickets").append("<div class='non-record'>Бронь не найдена</div>")
                }
            }
        });
        return false;
    })
}

function removeTicketInReservedPayment() {
    $("body").on("click", "#tickets .remove_ticket", function() {
        $(this).parent().parent().remove();
        recalculateTicketsPrices();
    });
}

function printTicketInReservedPayment() {
    $("body").on("click", "#tickets .print_ticket", function() {
        alert($(this).parent().parent().find(".ticket_id").val());
        ticket_id = $(this).parent().parent().find(".ticket_id").val();
        $.ajax({
            type: "POST",
            data: {"ticket_id": ticket_id},
            url: "/ajax_print_ticket",
            success: function (data) {
                if (data == "Success") {
                    window.open("/print_blank?ticket_id="+ticket_id, '_blank');
                }
            }
        });
    });
}

function recalculateTicketsPrices() {
    price = 0;
    $("#tickets td.price").each(function(){
        price = price + parseInt($(this).text());
    });
    $("#tickets .total").text(price);
}

function initializeBuyTickets() {
    $(".search_result form").submit(function(){
        var arr = [];
        $(".ticket_id").each(function(){ arr.push($(this).val()); })
        $.ajax({
            type: "POST",
            data: { "ticket_ids": arr.join(","), "payment_id": $("#tickets").attr("data-payment-id") },
            url: "/ajax_buy_ticket_by_ids",
            success: function (data) {
                $("#tickets .label").text("");
                $("#tickets .table .ticket").remove();
                $("#tickets .non-record").remove();
                $("#tickets").hide();
                $(".search_result input[type=submit]").hide();
                $("#tickets .non-record").text("Билеты успешно выкуплены");
            }
        });
        return false;
    });
}