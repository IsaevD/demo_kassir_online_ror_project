function kassirButtonInit() {
    $(".kassir_online_btn").click(function(){
        var event_id = parseInt($(this).data('event-id'));
        if (event_id) {
            url = "http://localhost:3000/master?event_id=" + event_id
        } else {
            url = "http://localhost:3000/master?event_id=empty"
        }
        $("body").append("<div class='kassir_popup'>" +
            "<div class='bg'></div>" +
            "<div class='section'>" +
            "<iframe id='kc_plugin' src='"+ url +"'></iframe>" +
            "</div>" +
            "</div>");
    });

    $(document).on('click', '.kassir_popup .bg, .kassir_popup .section', function() {
        $(".kassir_popup").remove();
    });
}
