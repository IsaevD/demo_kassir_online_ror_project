// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.cookie
//= require jquery_ujs
//= require zeroclipboard
//= require number_mask
//= require admin/interface
//= require schemas/schemas
//= require payments/payments
//= require plugin
//= require visual_interfaces_d/application
//= require maskedinput

$(function(){

    initializeVisualInterface();

    // Подключение интерфейса
    openTableFilterBar();
    openMenuItem();
    openLegendBar();
    removeTicketInPayment();
    printTicketInReservedPayment();
    initializePaymentForm();
    initializeDiscountForm();
    initializeEventItemForm();
    initPhoneMask();
    copyClipBoardInit();
    reportTogglerArrowInit();

    // Подключение расширенных схем залов
    if ($(".advanced_schema").length != 0) {
        initialAdvancedSchemaSeatClick();
        initialAdvancedSchemaChangeTicketStates();
        initialAdvancedSchemaChangeTicketPrices();
        initialAdvancedClearAllCheckedTickets();
        initialAdvancedSchemaChangeTicketDiscounts();
        initialUsualSchemaBuyClick();
        initialUsualSchemaBookClick();
    }

    if ($(".usual_schema").length != 0) {
        initialAdvancedSchemaSeatClick(true, true);
        initialAdvancedClearAllCheckedTickets();
        initialUsualSchemaBuyClick();
        initialUsualSchemaBookClick();
        intervalID = window.setInterval(function(){ refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id")); }, 4000);
    }

    if ($(".master_schema").length != 0) {
        initialAdvancedSchemaSeatClick(true, true, false);
        initialAdvancedClearAllCheckedTickets();
        initializeMasterBookTicket();
        intervalID = window.setInterval(function(){ refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id")); }, 4000);
    }

    if ($(".order_panel").length != 0) {
        initializePaymentSearch();
        removeTicketInReservedPayment();
        initializeBuyTickets();
    }

});


function initializePaymentForm() {
    if ($(".edit_payment").length != 0) {
        no_ignore_hidden = ["#payment_event_item_id", "#payment_updated_at", "#payment_number", "#payment_system_user_id"];
        no_ignore_hidden.forEach(function(entry) {
            $(entry).attr("disabled", true);
            $(entry).addClass("hidden");
        });
    }
}

function initializeEventItemForm() {
    if ($(".edit_event_item, .new_event_item").length != 0) {
        $("#event_item_content").parent().hide();
    }
    if ($(".edit_event_item").length != 0) {
        no_ignore_hidden = ["#event_item_hall_id", "#event_item_id"];
        no_ignore_hidden.forEach(function(entry) {
            $(entry).attr("disabled", true);
            $(entry).addClass("hidden");
        });
    }
    if ($(".new_event_item").length != 0) {
        no_ignore_hidden = ["#event_item_id"];
        no_ignore_hidden.forEach(function(entry) {
            $(entry).attr("disabled", true);
            $(entry).addClass("hidden");
        });
    }
}

function initializeDiscountForm() {
    if ($(".new_ticket_discount, .edit_ticket_discount").length != 0) {
        $("#ticket_discount_alias").parent().hide();
        initAutoComplete($("#ticket_discount_name"), $("#ticket_discount_alias"));
        initNumber(["#ticket_discount_value"]);
    }
}

function initAutoComplete(object, children) {
    $(object).change(function(){
        if($(children).val() == "") {
            if ($(object).val() != "") {
                $(children).val(transliterate($(object).val()));
            }
        }
    });
}

function transliterate(word){
    var answer = "", a = {};

    a["Ё"]="YO";a["Й"]="I";a["Ц"]="TS";a["У"]="U";a["К"]="K";a["Е"]="E";a["Н"]="N";a["Г"]="G";a["Ш"]="SH";a["Щ"]="SCH";a["З"]="Z";a["Х"]="H";a["Ъ"]="'";
    a["ё"]="yo";a["й"]="i";a["ц"]="ts";a["у"]="u";a["к"]="k";a["е"]="e";a["н"]="n";a["г"]="g";a["ш"]="sh";a["щ"]="sch";a["з"]="z";a["х"]="h";a["ъ"]="'";
    a["Ф"]="F";a["Ы"]="I";a["В"]="V";a["А"]="a";a["П"]="P";a["Р"]="R";a["О"]="O";a["Л"]="L";a["Д"]="D";a["Ж"]="ZH";a["Э"]="E";
    a["ф"]="f";a["ы"]="i";a["в"]="v";a["а"]="a";a["п"]="p";a["р"]="r";a["о"]="o";a["л"]="l";a["д"]="d";a["ж"]="zh";a["э"]="e";
    a["Я"]="Ya";a["Ч"]="CH";a["С"]="S";a["М"]="M";a["И"]="I";a["Т"]="T";a["Ь"]="'";a["Б"]="B";a["Ю"]="YU";
    a["я"]="ya";a["ч"]="ch";a["с"]="s";a["м"]="m";a["и"]="i";a["т"]="t";a["ь"]="'";a["б"]="b";a["ю"]="yu";
    a[" "]="_";a["%"]="";

    for (i in word){
        if (word.hasOwnProperty(i)) {
            if (a[word[i]] === undefined){
                answer += word[i];
            } else {
                answer += a[word[i]];
            }
        }
    }
    return answer;
}

function initNumber(object_arr) {
    object_arr.forEach(function(element) {
        $(element).numberMask({beforePoint:8});
    });
}

function initPhoneMask() {
    var inputs = [
        '.buttons input#phone'
    ];
    $.each( inputs , function ( index , value ) {
        $(value).mask('9 (999) 999 99 99');
    });
}

function send_ajax_request( url , data ) {
    $.ajax({
        url: url,
        type: "POST",
        cache: false,
        data: data,
        success: function(data){
            // add some code here
        }
    });
}
//= require turbolinks