// Функция интерфейса
function openTableFilterBar() {
    $(".open_bar").unbind("click");
    $(".open_bar").click(function(){
        if ($("#filter_form").is(":visible"))
            $("#filter_form").hide();
        else
            $("#filter_form").show();
    });
}

function openMenuItem() {
    $(".has_second_menu").unbind("click");
    $(".first_menu_item.has_second_menu").parent().click(function(){
        obj = $(this).children(".second_menu_item");
        if(obj.is(":visible")) {
            obj.hide();
            $(this).children(".first_menu_item").children(".icon-arrow-up").removeClass("icon-arrow-up").addClass("icon-arrow-down");
        }
        else {
            $(obj).show();
            $(this).children(".first_menu_item").children(".icon-arrow-down").removeClass("icon-arrow-down").addClass("icon-arrow-up");
        }
    });
}

function openLegendBar() {
    $("#legend_nav_panel").click(function(){
        if($("#legend").is(":visible")) {
            $("#legend").hide();
            $("#legend_nav_panel").children(".icon-arrow-up").removeClass("icon-arrow-up").addClass("icon-arrow-down");
        }
        else {
            $("#legend").show();
            $("#legend_nav_panel").children(".icon-arrow-down").removeClass("icon-arrow-down").addClass("icon-arrow-up");
        }
    })
}

function removeTicketInPayment() {
    if ($(".remove_ticket").length != 0) {
        $(".remove_ticket").click(function () {
            $(this).parent().parent().remove();
        })
    }
}

function copyClipBoardInit() {
    var clip = new ZeroClipboard($("#clipboard_button"))
    $('#clipboard_button').on('click', function( e ) {
        alert("Код кнопки успешно скопирован.");
        e.preventDefault();
    });
}

function reportTogglerArrowInit() {
    arrows = $('.report_table .arrow_toggler');
    if (arrows.length != 0) {
        $(document).on('click', arrows, function( event ) {
            var elem = $(event.target),
                table = elem.parent().parent().parent().parent(),
                cur_arrow = table.find('span.arrow_toggler'),
                table_body = table.children('tbody');
            cur_arrow.toggleClass( "packed" );
            cur_arrow.toggleClass( "unpacked" );
            table_body.toggle();
        });
    }
}