var order_cookie_param = 'planb_order';

// Получить ИД всех выбранных билетов схемы
function getActiveTicketsIds() {
    var seats = $(".hall_schema .places .seat.active");
    arrayIds = [];
    seats.each(function (index) {
        arrayIds.push($(this).data("ticket-id"));
    });
    return arrayIds
}

// Снять все отметки мест
function clearAllCheckedTickets() {
    var seats = $(".hall_schema .places .seat.active");
    seats.removeClass("active");
}

// Подсчитать количество выбранных билетов и итоговую сумму всех билетов
function caclucateSchemeCountAndTotal() {
    var count = 0, total = 0, seats = $(".hall_schema .places .seat.active");
    if (seats.length != 0) {
        seats.each(function (index) {
            total = total + parseInt($(this).data("price"));
            count = count + 1;
        });
        return [count, total]
    }
}

// Обновить информационное поле заказа
function updateOrderInfo(count, total) {
    var countBlock = $(".main_info .count");
    var totalBlock = $(".main_info .total");
    countBlock.text(count);
    totalBlock.text(total);
}

// Открыть информационное поле над выбранным местом
function openAdvancedTicketInfoWidget(seat) {
    seat.append("<div class='info_block'>" +
        "<div class='ticket_group'>Группа: <span class='value'>"+seat.attr('data-group')+"</span></div>" +
        "<div class='range'>Ряд: <span class='value'>"+seat.attr('data-range')+"</span></div>" +
        "<div class='place'>Место: <span class='value'>"+seat.attr('data-place')+"</span></div>" +
        "<div class='price'>Цена: <span class='value'>"+seat.attr('data-price')+"</span> руб.</div>" +
        "<div class='discount'>Скидка: <span class='value'>" + seat.attr('data-discount') + "</span></div>" +
        "<div class='state'>Статус: <span class='value'>"+seat.attr('data-state-name')+"</span></div>" +
        "</div>");
    if (seat.attr('data-user-id') != "")
        seat.children(".info_block").append("<div class='user'>Пользователь: <span class='value'><a target='_blank' href='/users/" + seat.attr('data-user-id') + "/edit'>" + seat.attr('data-user') + "</a></span></div>");
    else
        seat.children(".info_block").append("<div class='user'>Пользователь: <span class='value'>-</span></div>");

    if (seat.attr('data-system-user-id') != "")
        seat.children(".info_block").append("<div class='system-user'>Кассир: <span class='value'><a target='_blank' href='/system_users/"+seat.attr('data-system-user-id')+"/edit'>"+seat.attr('data-system-user')+"</a></span></div>");
    else
        seat.children(".info_block").append("<div class='system-user'>Кассир: <span class='value'>-</span></div>");

    if (seat.attr('data-payment-id') != "0")
        seat.children(".info_block").append("<div class='payment'>Платеж: <span class='value'><a target='_blank' href='/payments/"+seat.attr('data-payment-id')+"/edit'>"+seat.attr('data-payment-number')+"</a></span></div>");
    else
        seat.children(".info_block").append("<div class='payment'>Платеж: <span class='value'>-</span></div>");

}
// Закрыть информационное поле над выбранным местом
function closeAdvancedTicketInfoWidget() {
    var info_blocks = $(".hall_schema .places .seat .info_block");
    info_blocks.remove();
}

// Открыть информационное поле над выбранным местом (обычное представление)
function openUsualTicketInfoWidget(seat, add_active) {

    if (typeof(add_active) === "undefined") { add_active = true; }

    if (add_active) {
        seat.addClass("active");
    }

    seat.append("<div class='info_block usual_info'>" +
        "<div class='arrow'></div>" +
        "<div class='ticket_group'>Группа: <span class='value'>"+seat.attr('data-group')+"</span></div>" +
        "<div class='range'>Ряд: <span class='value'>"+seat.attr('data-range')+"</span></div>" +
        "<div class='place'>Место: <span class='value'>"+seat.attr('data-place')+"</span></div>" +
        "<div class='price'>Цена: <span class='value'>"+seat.attr('data-price')+"</span> руб.</div>");
}
// Закрыть информационное поле над выбранным местом (обычное представление)
function closeUsualTicketInfoWidget(seat, remove_active) {

    if (typeof(remove_active) === "undefined") { remove_active = true; }

    var info_blocks = $(".hall_schema .places .seat .info_block");
    info_blocks.remove();
    if (remove_active) {
        seat.removeClass("active");
    }
}
// Установить сообщение в виджете корзины в шапке
function setHeaderCartWidgetMessage(count, total) {
    $("#auth_form .in_cart_message").html("В вашей корзине <span class='attention count'>"+count+"</span> билетов на сумму <span class='attention price'>"+total+"</span> руб.");
}
// Установить сообщение в виджете корзины в схеме
function setSchemeCartWidgetMessage(count, total) {
    var schemeCartWidget = $(".cart_panel .price_values");
    if (schemeCartWidget.length != 0) {
        schemeCartWidget.children(".count").text(count);
        schemeCartWidget.children(".total").text(total);
    }
}


// Инициализация единичного клика по месту
function initialAdvancedSchemaSeatClick(usual_info_widget, update_widget, all_actions) {
    if (typeof(usual_info_widget) === "undefined") { usual_info_widget = false; }
    if (typeof(update_widget) === "undefined") { update_widget = false; }
    if (typeof(all_actions) === "undefined") { all_actions = true; }

    var seats = $(".hall_schema .places .seat");
    var first_seat = null;
    if (seats.length != 0) {
        if (usual_info_widget) {
            seats.hover(
                function(){
                    if ((!$(this).hasClass("non_active")) && ($(this).attr("data-state") == "available")) {
                        openUsualTicketInfoWidget($(this), false);
                    }
                },
                function(){
                    if ((!$(this).hasClass("non_active")) && ($(this).attr("data-state") == "available")) {
                        closeUsualTicketInfoWidget($(this), false);
                    }
                });
        } else {
            seats.hover(
                function(){
                    if (!$(this).hasClass("non_active")) {
                        openAdvancedTicketInfoWidget($(this));
                    }
                },
                function(){
                    if (!$(this).hasClass("non_active")) {
                        closeAdvancedTicketInfoWidget($(this));
                    }
                });
        }
        seats.click(function () {
            if (!$(this).hasClass("non_active")) {
                if ($(this).data("state") == "available") {
                    if(!$(this).hasClass("active")) {
                        $(this).addClass("active");
                        if (first_seat == null) {
                            first_seat = $(this);
                            setTimeout(function () {
                                first_seat = null;
                            }, 2000);
                        }
                        else {
                            if(all_actions) {
                                var second_seat = $(this);
                                $('.seat').filter(function () {
                                    var flag = false;
                                    if (($(".seat").index($(this)) > $(".seat").index(first_seat)) && ($(".seat").index($(this)) < $(".seat").index(second_seat))) {
                                        flag = true;
                                    }
                                    /*
                                    if (($(this).data("ticket-id") > first_seat.data("ticket-id")) && ($(this).data("ticket-id") < second_seat.data("ticket-id"))) {
                                        flag = true;
                                    }
                                    */
                                    if (flag) {
                                        if (!$(this).hasClass("non_active") && !$(this).hasClass("book_state") && !$(this).hasClass("buy_state")) {
                                            return $(this);
                                        }
                                    }

                                }).addClass('active');
                                first_seat = null;
                                second_seat = null;
                            }

                        }
                    } else {
                        $(this).removeClass("active");
                    }
                }
            }
            if (update_widget) {
                updateKassirWidgetInfo();
            }
        });
    }
}
// Инициализация единичного клика по месту
function initialUsualSchemaSeatClick() {
    var seats = $(".hall_schema .places .seat");
    var first_seat = null;
    if (seats.length != 0) {
        seats.click(function () {
            if ($(this).data("state") == "available") {
                if (!$(this).hasClass("active")) {
                    if ($(".places .seat.active").length == 5) {
                        // toDo: Сделать вывод нормального сообщения
                        //alert("Вы не можете купить/забронировать более чем 5 мест");
                        $(".collective_order").show();
                    } else {
                        closeUsualTicketInfoWidget($(this));
                        openUsualTicketInfoWidget($(this));

                        arr = caclucateSchemeCountAndTotal();
                        count = arr[0];
                        total = arr[1];

                        setSchemeCartWidgetMessage(count, total);
                        setHeaderCartWidgetMessage(count, total);
                    }
                } else {
                    closeUsualTicketInfoWidget($(this));

                    arr = caclucateSchemeCountAndTotal();
                    count = arr[0];
                    total = arr[1];

                    setSchemeCartWidgetMessage(count, total);
                    setHeaderCartWidgetMessage(count, total);
                }
            }
        });
    }
}

// Инициализация подтверждения заказа на схеме
function initialUsualSchemaSubmit() {
    var checkoutBtn = $("#checkout");
    if(checkoutBtn.length != 0) {
        checkoutBtn.click(function () {
            if(!$(this).hasClass("non_active")) {
                var activeSeats = $(".hall_schema .places .seat.active");
                if (activeSeats.length == 0) {
                    // toDO: Заменить выводом обычного сообщения
                    alert("Вы не выбрали билеты");
                    return false;
                } else {
                    setActiveTicketsInCookies(activeSeats);
                    return true;
                }
            }
        });
    }
}

// Установить выбранные места в cookies
function setActiveTicketsInCookies(seats) {
    arrayIds = [];
    seats.each(function (index) {
        arrayIds.push($(this).data("ticket-id"));
    });
    setTicketsCookie(arrayIds);
}

// Функции по работе с куки
function setTicketsCookie(ticketIds) {
    $.cookie(order_cookie_param, null, { expires: 3, path: '/' });
    $.cookie(order_cookie_param, ticketIds.join(","), { expires: 3, path: '/' });
}
function getTicketsCookie() {
    return $.cookie(order_cookie_param);
}
function initializeTicketsCookie() {
    $.cookie(order_cookie_param, null, { expires: 3, path: '/' });
}


// Инициализация формы смены статусов у билетов
function initialAdvancedSchemaChangeTicketStates() {
    var changeTicketStateForm = $("#change_state form");
    if (changeTicketStateForm.length != 0) {
        changeTicketStateForm.submit(function () {
            $.ajax({
                type: "POST",
                data: {"ticket_ids": getActiveTicketsIds().join(","), "event_id": $(".hall_schema").data("event-id"), "ticket_state_id": $(this).children("select").val() },
                url: "/ajax_change_ticket_states",
                success: function (data) {
                    if (data == "Success") {
                        eventId = $(".hall_schema").data("event-id");
                        refreshAdvancedSchemaByEventID(eventId);
                        alert("Статусы успешно обновлены");
                    }
                }
            });
            return false;
        });
    }
}

// Инициализация формы смены цены у билетов
function initialAdvancedSchemaChangeTicketPrices() {
    var changeTicketPriceForm = $("#change_price form");
    if (changeTicketPriceForm.length != 0) {
        changeTicketPriceForm.submit(function () {
            if($("#price").val() != "") {
                $.ajax({
                    type: "POST",
                    data: {"ticket_ids": getActiveTicketsIds().join(","), "price": $(this).children("#price").val()},
                    url: "/ajax_change_ticket_prices",
                    success: function (data) {
                        if (data == "Success") {
                            eventId = $(".hall_schema").data("event-id");
                            refreshAdvancedSchemaByEventID(eventId);
                            $(".seat").removeClass("active");
                            alert("Цены успешно обновлены");
                        }
                    }
                });
            }
            return false;
        });
    }
}

// Инициализация формы смены скидок у билетов
function initialAdvancedSchemaChangeTicketDiscounts() {
    var changeTicketDiscountForm = $("#change_discount form");
    if (changeTicketDiscountForm.length != 0) {
        changeTicketDiscountForm.submit(function () {
            $.ajax({
                type: "POST",
                data: {"ticket_ids": getActiveTicketsIds().join(","), "ticket_discount_id": $(this).children("select").val() },
                url: "/ajax_change_ticket_discounts",
                success: function (data) {
                    if (data == "Success") {
                        eventId = $(".hall_schema").data("event-id");
                        refreshAdvancedSchemaByEventID(eventId);
                        $(".seat").removeClass("active");
                        alert("Скидки успешно обновлены");
                    }
                }
            });
            return false;
        });
    }
}

// Инициализация действия очистки схемы от выбранных мест
function initialAdvancedClearAllCheckedTickets() {
    $("#clear_checked_tickets").click(function(){
        clearAllCheckedTickets();
    })
}

// Обновление схемы по идентификатору события
function refreshAdvancedSchemaByEventID(eventId) {
    $.ajax({
        type: "POST",
        data: { "event_id": eventId },
        url: "/ajax_get_tickets_info",
        success: function (data) {
            data.forEach(function(item, i, arr) {
                obj = $("li.seat[data-seat-id="+item[0]+"]").first();
                if (obj.length != 0) {
                    obj.attr("data-state", item[1]);
                    obj.attr("data-price", item[2]);
                    obj.attr("data-state-alias", item[3]);
                    obj.attr("data-discount", item[5]);
                    obj.attr("data-payment-id", item[6]);
                    obj.attr("data-payment-number", item[7]);
                    obj.attr("data-user-id", item[8]);
                    obj.attr("data-user", item[9]);
                    obj.attr("data-system-user-id", item[10]);
                    obj.attr("data-system-user", item[11]);
                    obj.attr("data-ticket-number", item[12]);
                    obj.css("background-color", item[4]);
                }
            });
        }
    });
}

// Обновление схемы по идентификатору события
function refreshUsualSchemaByEventID(eventId) {
    $.ajax({
        type: "POST",
        data: { "event_id": eventId },
        url: "/ajax_usual_get_tickets_info",
        success: function (data) {
            data.forEach(function(item, i, arr) {
                obj = $("li.seat[data-seat-id="+item[0]+"]").first();
                if (obj.length != 0) {
                    obj.attr("data-state", item[1]);
                    obj.attr("data-price", item[2]);
                    obj.attr("data-state-alias", item[3]);
                    obj.attr("data-discount", item[5]);
                    obj.css("background-color", item[4]);
                }
            });
        }
    });
}


// Обновление виджета с информацией о выбранных билетов
function updateKassirWidgetInfo() {
    var seats = $(".hall_schema .places .seat.active");
    var total = 0;
    var count = 0;
    seats.each(function(){
        total = total + parseInt($(this).attr("data-price"));
        count = count + 1;
    });
    $(".order_info .count .value").text(count);
    $(".order_info .total .value").text(total);
}



// Инициализация клика по кнопке "Купить"
function initialUsualSchemaBuyClick() {
    var buyBtn = $("#buy_ticket");
    if (buyBtn.length != 0) {
        buyBtn.click(function () {
            if ($(".places li.active").length != 0) {
                $.ajax({
                    type: "POST",
                    data: {
                        "ticket_ids": getActiveTicketsIds().join(","),
                        "event_id": $(".hall_schema").attr("data-event-id")
                    },
                    url: "/ajax_create_payment_and_buy_ticket_by_ids",
                    success: function (data) {
                        if (data == "Success") {
                            refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
                            alert("Билеты успешно куплены");
                            $(".seat.active").removeClass("active");
                            $(".order_info .count .value").text("0");
                            $(".order_info .total .value").text("0");
                        }
                    }
                });
            } else {
                alert("Места не выбраны");
            }
        });
    }
}

// Инициализация клика по кнопке "Забронировать"
function initialUsualSchemaBookClick() {
    var bookBtn = $("#book_ticket");
    if (bookBtn.length != 0) {
        bookPopup();
        bookBtn.click(function () {
            if ($(".places li.active").length != 0) {
                $("#book_popup").show();
                /*
                $.ajax({
                    type: "POST",
                    data: {"ticket_ids": getActiveTicketsIds().join(","), "event_id": $(".hall_schema").attr("data-event-id") },
                    url: "/ajax_create_payment_and_book_ticket_by_ids",
                    success: function (data) {
                        if (data == "Success") {
                            refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
                            $(".order_info .count .value").text("0");
                            $(".order_info .total .value").text("0");
                        }
                    }
                });
                */
            } else {
                alert("Места не выбраны");
            }
        });
    }
}

function bookPopup() {
    $("#book").click(function(){
        $.ajax({
            type: "POST",
            data: {"ticket_ids": getActiveTicketsIds().join(","), "event_id": $(".hall_schema").attr("data-event-id"), "phone": $("#phone").val(), "fio": $("#fio").val() },
            url: "/ajax_create_payment_and_book_ticket_by_ids",
            success: function (data) {
                if (data == "Success") {
                    refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
                    $(".order_info .count .value").text("0");
                    $(".order_info .total .value").text("0");
                    $("#book_popup").hide();
                }
            }
        });
    });
    $("#close").click(function(){
        $("#book_popup").hide();
    });
}

// Обновление состояния всех мест для расширенной схемы
function refreshAdvancedSeatsStatesByEventID(eventId) {
    $.ajax({
        type: "POST",
        data: { "event_id": eventId },
        url: "/ajax_admin_get_tickets",
        success: function (data) {
            data.forEach(function(item, i, arr) {
                obj = $("li.seat[data-seat-id="+item[0]+"]").first();
                if (obj.length != 0) {
                    obj.attr("data-state", item[1]);
                }
            });
        }
    });
}

// Покупка билетов с распечаткой бланков
function initializePrintBlanksPopup() {
    $("#buy_ticket .btn").click(function(){
        if ($(".hall_schema .places .seat.active").length != 0) {
            if (!$(".accepts").is(":visible")) {
                $(".accepts").show();
                total = 0;
                $(".popup_cnt .table tbody tr").remove();
                $(".hall_schema .places .seat.active").each(function () {
                    total = total + parseInt($(this).attr("data-price"));
                    $(".popup_cnt .table tbody").append('<tr class="ticket" data-print="0" data-ticket-id="' + $(this).attr("data-ticket-id") + '"><td>' + $(this).attr("data-group") + '</td><td>' + $(this).attr("data-range") + '</td><td>' + $(this).attr("data-place") + '</td><td>' + $(this).attr("data-discount") + '</td><td>' + $(this).attr("data-price") + ' руб.</td><td><div class="btn blue_btn print_ticket">Печать</div></td></tr>');
                });
            }
        } else
            alert("Билеты не выбраны");
    });
    $("body").on("click", ".print_ticket", function(){
        ticket_id = $(this).parent().parent().attr("data-ticket-id");
        btn = $(this);
        $.ajax({
            type: "POST",
            data: { "ticket_id": ticket_id },
            url: "/ajax_print_ticket",
            success: function (data) {
                if (data == "Success") {
                    btn.text("Перепечатать");
                    window.open("/print_blank?ticket_id="+ticket_id, '_blank');
                }
            }
        });
    });
    /*
    $("body").on("click", ".reject_ticket", function(){
        if ($(this).hasClass("blue_btn")) {
            ticket_id = $(this).parent().parent().attr("data-ticket-id");
            btn = $(this);
            $.ajax({
                type: "POST",
                data: { "ticket_id": ticket_id },
                url: "/admin/ajax_defect_ticket",
                success: function (data) {
                    if (data == "Success") {
                        btn.removeClass("blue_btn").addClass("grey_btn");
                        btn.parent().children(".print_ticket").removeClass("grey_btn").addClass("blue_btn");
                    }
                }
            });

        }
    });
    */
    $("#accept_tickets").click(function(){
        $.ajax({
            type: "POST",
            data: {"ticket_ids": getActiveTicketsIds().join(","), "event_id": $(".hall_schema").attr("data-event-id") },
            url: "/ajax_buy_ticket",
            success: function (data) {
                if (data == "Success") {
                    refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
                    $(".seat.active").removeClass("active");
                    $(".accepts").hide();
                }
            }
        });
    });

    $("#abort_tickets").click(function(){
        $(".accepts").hide();
    });
}


function initializeAdvancedBookTicket() {
    $("#book_ticket .btn").click(function(){
        if ($(".hall_schema .places .seat.active").length != 0) {
            $.ajax({
                type: "POST",
                data: {
                    "ticket_ids": getActiveTicketsIds().join(","),
                    "event_id": $(".hall_schema").attr("data-event-id")
                },
                url: "/ajax_book_ticket",
                success: function (data) {
                    if (data == "Success") {
                        refreshUsualSchemaByEventID($(".hall_schema").attr("data-event-id"));
                        $(".seat.active").removeClass("active");
                        alert("Билеты успешно забронированы");
                    }
                }
            });
        } else {
            alert("Места не выбраны")
        }
    });
}

function initializeMasterBookTicket() {

    $("#bro_next").click(function(){
        if ($("#name").val() == "") {
            alert("Введите имя");
        } else {
            if ($("#phone").val() == "") {
                alert("Введите номер телефона");
            } else {
                if ($(".hall_schema .places .seat.active").length != 0) {
                    $("#name").attr('disabled', 'disabled');
                    $("#phone").attr('disabled', 'disabled');
                    $("#bro_next").hide();
                    $.ajax({
                        type: "POST",
                        data: {
                            "ticket_ids": getActiveTicketsIds().join(","),
                            "event_id": $(".hall_schema").attr("data-event-id"),
                            "email": $("#email").val(),
                            "fio": $("#name").val()
                        },
                        url: "/ajax_master_create_payment_and_book_ticket_by_ids",
                        success: function (data) {

                            $(".hall_schema").hide();
                            $(".triumph").show();
                            $("#phone_v").text(data.email);
                            $("#number_v").text(data.payment_num);
                            $(".triumph .text").text(data.message);
                        }
                    });
                } else {
                    alert("Места не выбраны");
                }
            }
        }
    });
}