class Hall < ActiveRecord::Base

  belongs_to :schema_item

  VALID_ALIAS_REGEX = /[a-zA-Z]+/

  validates :alias,
            presence: true,
            length: { minimum: 4, maximum: 50},
            uniqueness: true,
            format: { with: VALID_ALIAS_REGEX }

  validates :name,
            presence: true,
            length: { minimum: 4, maximum: 255},
            uniqueness: true

end
