class SeatItemType < ActiveRecord::Base

  def self.get_color_by_price(price)
    type = SeatItemType.where("price <= #{price.to_i}").order(:price => :desc).first()
    if type.nil?
      type = SeatItemType.all.order(:price => :desc).first()
    end
    return type.color
  end

end
