class EventItem < ActiveRecord::Base

  belongs_to :hall
  has_many :payments, dependent: :destroy
  has_many :tickets, dependent: :destroy
  has_many :ticket_discount, dependent: :destroy

  has_attached_file :image, :styles => { :card => "662x350", :list => "480x200", :preview => "128x55" , :thumb => "100x100" }, :default_url => "/images/system_main_items/no_user_avatar.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :name,
            presence: true,
            length: { minimum: 4, maximum: 255}

  validates :description,
            presence: true

  validates :date,
            presence: true

end
