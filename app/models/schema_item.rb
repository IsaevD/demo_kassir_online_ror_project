class SchemaItem < ActiveRecord::Base

  has_many :sectors
  has_many :seat_items

end
