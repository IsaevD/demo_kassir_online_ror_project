class DeliveryMethod < ActiveRecord::Base

  def self.COURIER_ALIAS
    return "courier"
  end

  def self.HIMSELF_ALIAS
    return "himself"
  end

  def self.courier_state
    return self.find_by(:alias => self.COURIER_ALIAS)
  end
  def self.himself_state
    return self.find_by(:alias => self.HIMSELF_ALIAS)
  end

end
