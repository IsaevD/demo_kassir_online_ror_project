class SchemaType < ActiveRecord::Base

  def self.SINGLE_LEVEL_ALIAS
    return "single_level"
  end

  def self.MULTI_LEVEL_ALIAS
    return "multi_level"
  end

  def self.single_level
    return self.find_by(:alias => self.SINGLE_LEVEL_ALIAS)
  end

  def self.multi_level
    return self.find_by(:alias => self.MULTI_LEVEL_ALIAS)
  end

end
