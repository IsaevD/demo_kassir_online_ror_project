class PaymentType < ActiveRecord::Base

  def self.PURCHASE_ALIAS
    return "purchase"
  end

  def self.RESERVATION_ALIAS
    return "reservation"
  end

  def self.purchase_state
    return self.find_by(:alias => self.PURCHASE_ALIAS)
  end
  def self.reservation_state
    return self.find_by(:alias => self.RESERVATION_ALIAS)
  end


end
