class TicketDiscount < ActiveRecord::Base

  def self.get_price_by_discount(ticket)
    discount = ticket.ticket_discount
    price = ticket.price
    if discount.nil?
      return price
    else
      if discount.date >= Time.now
        if discount.is_percent
          return (price - price * (discount.value.to_f / 100)).round
        else
          return (price - discount.value).round
        end
      else
        return price
      end
    end
  end

  def self.get_price_by_discount_without_date_link(ticket)
    discount = ticket.ticket_discount
    price = ticket.price
    if discount.is_percent
      return (price - price * (discount.value.to_f / 100)).round
    else
      return (price - discount.value).round
    end
  end

end
