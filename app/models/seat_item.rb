class SeatItem < ActiveRecord::Base
  belongs_to :seat_item_type
  belongs_to :sector_group
  has_many :tickets
end
