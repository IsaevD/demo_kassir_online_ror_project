class TicketState < ActiveRecord::Base

  def self.AVAILABLE_ALIAS
    return "available"
  end

  def self.DISABLED_ALIAS
    return "desktop"
  end

  def self.BOOKED_ALIAS
    return "booked"
  end

  def self.BOUGHT_ALIAS
    return "bought"
  end

  def self.available_state
    return self.find_by(:alias => self.AVAILABLE_ALIAS)
  end
  def self.disabled_state
    return self.find_by(:alias => self.DISABLED_ALIAS)
  end
  def self.booked_state
    return self.find_by(:alias => self.BOOKED_ALIAS)
  end
  def self.bought_state
    return self.find_by(:alias => self.BOUGHT_ALIAS)
  end


end
