class Ticket < ActiveRecord::Base

  belongs_to :ticket_state
  belongs_to :seat_item
  belongs_to :event_item
  belongs_to :user
  belongs_to :system_user
  belongs_to :ticket_discount
  belongs_to :payment

  # Генерация номера обычного билета
  def self.create_ticket_normal_number(ticket, user = nil)
    counter = Setting.generate_normal_counter_number
    ticket.number = "#{counter} #{Setting.get_setting(Setting.NORMAL_COUNTER_PREFIX_ALIAS)}"
    Setting.set_setting(Setting.NORMAL_COUNTER_NUMBER_ALIAS, counter)
    if !user.nil?
      kassir = Kassir.find_by(:system_user_id => user.id)
      if !kassir.nil?
        kassir.normal_counter = kassir.normal_counter - 1
      end
    end
    return ticket
  end
  # Генерация номера фирменного билета
  def self.create_ticket_special_number(ticket)
    counter = Setting.generate_special_counter_number
    ticket.number = "#{counter} #{Setting.get_setting(Setting.SPECIAL_COUNTER_PREFIX_ALIAS)}"
    Setting.set_setting(Setting.SPECIAL_COUNTER_NUMBER_ALIAS, counter)
    return ticket
  end
  # Формирование платежа на покупку билетов
  def self.create_purchase_payment(tickets, event_id, user, ignore_number = true)
    ticket_state = TicketState.bought_state
    payment = Payment.create_payment(nil, user.id, event_id, PaymentType.purchase_state)
    tickets.each do |ticket|
      ticket.ticket_state = ticket_state
      if !ignore_number
        ticket = self.create_ticket_normal_number(ticket, user)
      else
        if ticket.number.nil?
          ticket = self.create_ticket_normal_number(ticket, user)
        end
      end
      ticket.payment_id = payment.id
      ticket.save
    end
  end
  # Формирование платежа на бронирование билетов
  def self.create_reservation_payment(tickets, event_id, user)
    ticket_state = TicketState.booked_state
    payment = Payment.create_payment(nil, user.id, event_id, PaymentType.reservation_state)
    tickets.each do |ticket|
      ticket.ticket_state = ticket_state
      ticket.payment_id = payment.id
      ticket.save
    end
  end

  def self.get_discount_name(ticket)
    discount = ticket.ticket_discount
    if discount.nil?
      return "Нет"
    else
      return discount.name
    end
  end

  def self.get_actual_price(ticket)
    return TicketDiscount.get_price_by_discount(ticket)
  end

  def self.book_ticket_by_user(ticket, user)
    ticket.user = @current_user
    ticket.ticket_state = TicketState.booked_state
    ticket.save
    return nil
  end

  def self.buy_ticket_by_user(ticket, user)
    ticket.user = @current_user
    ticket.ticket_state = TicketState.bought_state
    ticket.save
  end

  def self.get_tickets_by_event_id(event_id)
    return self.where(:event_item_id => event_id)
  end

  def self.get_tickets_by_ids(ids)
    if ids.is_a? String
      ids = ids.split(",")
    end
    return Ticket.where("id in (?)", ids)
  end

  def self.get_tickets_main_info(ids)
    if !ids.nil?
      count = 0
      price = 0
      self.get_tickets_by_ids(ids).each do |ticket|
        #if !ticket.user.nil?
        #  if ticket.user.id != @current_user.id
            count = count + 1
            price = price + ticket.price
            #tickets.push(ticket.id)
        #  end
        #end
      end
      return [count, price]#, tickets]
    else
      return [0, 0, nil]
    end
  end

  def self.get_ticket_price(event_id, seat_id)
    ticket = self.where(:event_item_id => event_id, :seat_id => seat_id).first()
    if !ticket.nil?
      return ticket.price
    end
  end

end
