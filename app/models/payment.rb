class Payment < ActiveRecord::Base

  has_many :tickets
  belongs_to :payment_type
  belongs_to :user
  belongs_to :system_user
  belongs_to :event_item

  def self.generate_number
    SecureRandom.hex(3)
  end

  def self.create_payment(user_id, system_user_id, event_id, payment_type, delivery_method = nil, delivery_phone = nil, delivery_address = nil, fio = nil, email = nil)
    payment = Payment.new({
      :number => self.generate_number,
      :user_id => user_id,
      :system_user_id => system_user_id,
      :event_item_id => event_id,
      :payment_type_id => payment_type.id,
      :delivery_method_id => !delivery_method.nil? ? delivery_method.id : nil,
      :phone => delivery_phone,
      :delivery_address => delivery_address,
      :fio => fio,
      :email => email
    })
    payment.save
    return payment
  end

  def self.get_payments_by_user(user)
    if !user.nil?
      payments = Payment.where(:user_id => user.id).order(:created_at => :desc)
      if payments.count == 0
        return nil
      else
        return payments
      end
    else
      return nil
    end
  end

end
