Rails.application.routes.draw do

  scope module: "desktop" do
    root "index#index"
  end

  mount ModulesD::Engine => "/"
  mount SystemMainItemsD::Engine => "/"
  mount SystemUsersAuthorizeD::Engine => "/"
  mount AccessRightsD::Engine => "/"
  mount SystemUserActionsLogD::Engine => "/"
  scope module: "admin" do
    resources :halls
    resources :event_items
    resources :ticket_discounts
    resources :ticket_states
    resources :seat_item_types
    resources :payments

    match '/test', to: "index#test", via: "get"
    match '/master', to: "index#master", via: "get"
    match '/payments/:id', to: "payments#update", via: "post"
    match '/main_page', to: "index#index", via: "get"
    match '/event_items/extend/:event_id', to: "event_items#extend", via: "get"
    match '/schemas/:event_id', to: "schemas#index", via: "get"
    match '/only_schema/:event_id', to: "schemas#only_schema", via: "get"
    match '/ajax_book_ticket_by_ids', to: "schemas#ajax_book_ticket_by_ids", via: "post"
    match '/ajax_get_ticket_by_id', to: "schemas#ajax_get_ticket_by_id", via: "post"
    match '/ajax_admin_get_tickets', to: "schemas#ajax_get_tickets", via: "post"

    # Расширенные схемы
    match '/advanced_schemas/:event_id', to: "advanced_schemas#index", via: "get"
    match '/ajax_change_ticket_states', to: "advanced_schemas#ajax_change_ticket_states", via: "post"
    match '/ajax_change_ticket_prices', to: "advanced_schemas#ajax_change_ticket_prices", via: "post"
    match '/ajax_get_tickets_info', to: "advanced_schemas#ajax_get_tickets_info", via: "post"
    match '/ajax_change_ticket_discounts', to: "advanced_schemas#ajax_change_ticket_discounts", via: "post"
    match '/ajax_buy_ticket', to: "advanced_schemas#ajax_buy_ticket", via: "post"
    match '/ajax_book_ticket', to: "advanced_schemas#ajax_book_ticket", via: "post"
    match '/ajax_print_ticket', to: "advanced_schemas#ajax_print_ticket", via: "post"

    # Кассир
    match '/ajax_get_reservation_payment_by_number', to: "schemas#ajax_get_reservation_payment_by_number", via: "post"
    match '/ajax_buy_ticket_by_ids', to: "schemas#ajax_buy_ticket_by_ids", via: "post"
    match '/ajax_create_payment_and_buy_ticket_by_ids', to: "schemas#ajax_create_payment_and_buy_ticket_by_ids", via: "post"
    match '/ajax_create_payment_and_book_ticket_by_ids', to: "schemas#ajax_create_payment_and_book_ticket_by_ids", via: "post"
    match '/ajax_master_create_payment_and_book_ticket_by_ids', to: "schemas#ajax_master_create_payment_and_book_ticket_by_ids", via: "post"
    match '/ajax_usual_get_tickets_info', to: "schemas#ajax_usual_get_tickets_info", via: "post"
    match '/accept_blank_request', to: "schemas#accept_blank_request", via: "get"
    match '/accept_blank_request', to: "schemas#accept_blank_request", via: "post"
    match '/print_blank', to: "schemas#print_blank", via: "get"

    #отчеты
    match '/sales_report', to: 'reports#sales_report', via: "get"
    match '/sales_report', to: 'reports#sales_report', via: "post"

    # информация по событию
    match '/event_data', to: 'index#get_event_data', via: 'post'
  end

end
