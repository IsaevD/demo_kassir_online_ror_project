require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Builds
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.activate_log = true
    config.i18n.default_locale = :ru
    config.paginator_per_page = 10
    config.after_sign_in_path = "/main_page"
    config.weekdays = ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "CБ", "ВС"]
    config.months = {
        "January" => "Январь",
        "February" => "Февраль",
        "March" => "Март",
        "April" => "Апрель",
        "May" => "Май",
        "June" => "Июнь",
        "July" => "Июль",
        "August" => "Август",
        "September" => "Сентябрь",
        "October" => "Октябрь",
        "November" => "Ноябрь",
        "December" => "Декабрь"
    }

  end
end
