# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160127091919) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access_rights_d_access_rights", force: true do |t|
    t.integer  "system_user_role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "access_rights_d_module_rights", force: true do |t|
    t.integer  "access_right_id"
    t.integer  "module_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delivery_methods", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_items", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "content"
    t.datetime "date"
    t.integer  "hall_id"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "halls", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "schema_item_id"
  end

  create_table "modules_d_module_items", force: true do |t|
    t.string   "alias"
    t.string   "module_name"
    t.string   "name"
    t.text     "description"
    t.string   "git_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payments", force: true do |t|
    t.string   "number"
    t.integer  "user_id"
    t.integer  "system_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_item_id"
    t.integer  "payment_type_id"
    t.string   "phone"
    t.string   "delivery_address"
    t.integer  "delivery_method_id"
    t.string   "fio"
    t.string   "email"
  end

  create_table "schema_items", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "schema_type_id"
    t.integer  "hall_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "schema_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seat_item_types", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.string   "color"
    t.integer  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seat_items", force: true do |t|
    t.string   "row"
    t.string   "number"
    t.integer  "sector_group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "schema_item_id"
    t.integer  "seat_item_type_id"
    t.string   "special"
  end

  create_table "sector_groups", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "sector_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sectors", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "schema_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_main_items_d_system_states", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_main_items_d_system_user_roles", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.text     "description"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_main_items_d_system_users", force: true do |t|
    t.string   "login"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_token"
    t.integer  "system_user_role_id"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "system_user_actions_log_d_actions_log_records", force: true do |t|
    t.integer  "system_user_id"
    t.integer  "log_record_state_id"
    t.datetime "action_date"
    t.integer  "module_item_id"
    t.integer  "entity_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_user_actions_log_d_log_record_states", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ticket_discounts", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.integer  "value"
    t.boolean  "is_percent"
    t.integer  "system_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_item_id"
    t.datetime "date"
  end

  create_table "ticket_states", force: true do |t|
    t.string   "alias"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tickets", force: true do |t|
    t.integer  "price"
    t.integer  "ticket_state_id"
    t.integer  "event_item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "seat_item_id"
    t.integer  "ticket_discount_id"
    t.integer  "payment_id"
  end

end
