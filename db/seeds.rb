ModulesD::Engine.load_seed
SystemMainItemsD::Engine.load_seed
AccessRightsD::Engine.load_seed
SystemUserActionsLogD::Engine.load_seed

# Импорт информации о модулях
modules = {
    "articles" => {
        :module_name => "admin",
        :name => "Статьи",
        :description => "Предназначен для управления статьями",
        :git_url => ""
    },
    "requests" => {
        :module_name => "admin",
        :name => "Заявки",
        :description => "Предназначен для управления заявками",
        :git_url => ""
    },
    "halls" => {
        :module_name => "admin",
        :name => "Площадки",
        :description => "Предназначен для управления площадками",
        :git_url => ""
    },
    "event_items" => {
        :module_name => "admin",
        :name => "Мероприятия",
        :description => "Предназначен для управления мероприятиями",
        :git_url => ""
    },
    "payments" => {
        :module_name => "admin",
        :name => "Платежи",
        :description => "Предназначен для управления платежами",
        :git_url => ""
    },
    "seat_item_types" => {
        :module_name => "admin",
        :name => "Ценовые группы",
        :description => "Предназначен для управления ценновыми группами на билеты",
        :git_url => ""
    },
    "ticket_discounts" => {
        :module_name => "admin",
        :name => "Скидки на билеты",
        :description => "Предназначен для управления скидками на билеты",
        :git_url => ""
    }
}
modules.each do |key, value|
  if (ModulesD::ModuleItem.find_by(:alias => key).nil?)
    ModulesD::ModuleItem.new({ :alias => key, :module_name => value[:module_name], :name => value[:name], :description => value[:description], :git_url => value[:git_url] }).save
  end
end

# Импорт ролей пользователей
puts "--- System User Roles start import"
system_user_roles = {
    "administrator" => {
        :name => "Администратор",
        :description => "Управляет всеми модулями сайта по публикации контента и решения бизнес-задач"
    },
    "kassir" => {
        :name => "Кассир",
        :description => "Управляет модулями сайта по покупки билетов"
    }
}
system_user_roles.each do |key, value|
  if (SystemMainItemsD::SystemUserRole.find_by(:alias => key).nil?)
    SystemMainItemsD::SystemUserRole.new({ :alias => key, :name => value[:name], :description => value[:description], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
  end
end
puts "+++ System User Roles import success"
puts "*****"
puts "--- System Users start import"
system_users = {
    "administrator" => {
        :first_name => "Иван",
        :middle_name => "Иванович",
        :last_name => "Иванов",
        :email => "default_administrator@test.com",
        :system_user_role_alias => "administrator"
    },
    "kassir" => {
        :first_name => "Петр",
        :middle_name => "Петрович",
        :last_name => "Петров",
        :email => "default_kassir@test.com",
        :system_user_role_alias => "kassir"
    }
}
system_users.each do |key, value|
  if (SystemMainItemsD::SystemUser.find_by(:login => key).nil?)
    system_user = SystemMainItemsD::SystemUser.new({
         :login => key,
         :first_name => value[:first_name],
         :middle_name => value[:middle_name],
         :last_name => value[:last_name],
         :email => value[:email],
         :system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => value[:system_user_role_alias]).id,
         :system_state_id => SystemMainItemsD::SystemState.active_state.id
     })
    system_user.password = "123456"
    system_user.password_confirmation = "123456"
    system_user.save
  end
end


puts "Custom modules import success"
puts "*****"
puts "--- Payment Types start import"
# Импорт городов
payment_types = {
    "purchase" => {
        :name => "Покупка"
    },
    "reservation" => {
        :name => "Бронирование"
    }
}
payment_types.each do |key, value|
  if (PaymentType.find_by(:alias => key).nil?)
    PaymentType.new({ :alias => key, :name => value[:name] }).save
  end
end
puts "+++ Payment Types import success"
puts "*****"

puts "--- Delivery Methods start import"
# Импорт способов доставки
delivery_methods = {
    "courier" => {
        :name => "Курьер"
    },
    "himself" => {
        :name => "Самостоятельно через точку продаж"
    }
}
delivery_methods.each do |key, value|
  if (DeliveryMethod.find_by(:alias => key).nil?)
    DeliveryMethod.new({ :alias => key, :name => value[:name] }).save
  end
end
puts "+++ Delivery Methods import success"

puts "*****"
puts "--- Halls start import"
halls = {
    "main_hall" => {
        :name => "Главный зал"
    },
    "main_hall_with_seats" => {
        :name => "Главный зал расширенный"
    },
    "theatr_hall" => {
        :name => "Театральный зал"
    }
}
halls.each do |key, value|
  if (Hall.find_by(:alias => key).nil?)
    Hall.new({ :alias => key, :name => value[:name], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
  end
end
puts "+++ Halls import success"
puts "*****"

# Импорт типов мест
ticket_states = {
    "available" => {
        :name => "Доступно"
    },
    "booked" => {
        :name => "Забронировано"
    },
    "bought" => {
        :name => "Куплено"
    },
}
ticket_states.each do |key, value|
  if (TicketState.find_by(:alias => key).nil?)
    TicketState.new({ :alias => key , :name => value[:name] }).save
  end
end
puts "Ticket states import success"

# Импорт типов схем
schema_types = {
    "single_level" => {
        :name => "Одноуровневая"
    },
    "multi_level" => {
        :name => "Многоуровневая"
    }
}
schema_types.each do |key, value|
  if (SchemaType.find_by(:alias => key).nil?)
    SchemaType.new({ :alias => key , :name => value[:name] }).save
  end
end
puts "Schema types import success"
# Импорт типов мест
seat_types = {
    "from200" => {
        :name => "От 200 руб.",
        :price => 200,
        :color => "#e76aa2"
    },
    "from300" => {
        :name => "От 300 руб.",
        :price => 300,
        :color => "#df007d"
    },
    "from400" => {
        :name => "От 400 руб.",
        :price => 400,
        :color => "#f81227"
    },
    "from500" => {
        :name => "От 500 руб.",
        :price => 500,
        :color => "#e57a1a"
    },
    "from600" => {
        :name => "От 600 руб.",
        :price => 600,
        :color => "#fcc705"
    },
    "from700" => {
        :name => "От 700 руб.",
        :price => 700,
        :color => "#d9df84"
    },
    "from800" => {
        :name => "От 800 руб.",
        :price => 800,
        :color => "#adc920"
    },
    "from900" => {
        :name => "От 900 руб.",
        :price => 900,
        :color => "#019544"
    },
    "from1000" => {
        :name => "От 1000 руб.",
        :price => 1000,
        :color => "#02773b"
    },
    "from1100" => {
        :name => "От 1100 руб.",
        :price => 1100,
        :color => "#56a5b2"
    },
    "from1200" => {
        :name => "От 1200 руб.",
        :price => 1200,
        :color => "#0a99e7"
    },
    "from1300" => {
        :name => "От 1300 руб.",
        :price => 1300,
        :color => "#0b99e5"
    },
    "from1400" => {
        :name => "От 1400 руб.",
        :price => 1400,
        :color => "#006289"
    },
    "from1500" => {
        :name => "От 1500 руб.",
        :price => 1500,
        :color => "#7c70b0"
    }
}
seat_types.each do |key, value|
  if (SeatItemType.find_by(:alias => key).nil?)
    SeatItemType.new({ :alias => key , :name => value[:name], :price => value[:price], :color => value[:color] }).save
  end
end
from200_obj = SeatItemType.find_by(:alias => "from200").id
from300_obj = SeatItemType.find_by(:alias => "from300").id
from400_obj = SeatItemType.find_by(:alias => "from400").id
from500_obj = SeatItemType.find_by(:alias => "from500").id
from600_obj = SeatItemType.find_by(:alias => "from600").id
from700_obj = SeatItemType.find_by(:alias => "from700").id
from800_obj = SeatItemType.find_by(:alias => "from800").id
from900_obj = SeatItemType.find_by(:alias => "from900").id
from1000_obj = SeatItemType.find_by(:alias => "from1000").id
from1100_obj = SeatItemType.find_by(:alias => "from1100").id
from1200_obj = SeatItemType.find_by(:alias => "from1200").id
from1300_obj = SeatItemType.find_by(:alias => "from1300").id
from1400_obj = SeatItemType.find_by(:alias => "from1400").id
from1500_obj = SeatItemType.find_by(:alias => "from1500").id
puts "Schema types import success"


# Импорт схемы "Главный зал"
puts "Schema Main Hall start import"
main_hall_schema = {
    "main_hall" => {
        :name => "Главный зал",
        :schema_type_id => SchemaType.single_level.id,
        :hall_id => Hall.find_by(:alias => "main_hall").id
    }
}
main_hall_schema.each do |key, value|
  if (SchemaItem.find_by(:alias => key).nil?)
    schema_item = SchemaItem.new({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
    schema_item.save
    hall = Hall.find_by(:alias => "main_hall")
    hall.schema_item_id = schema_item.id
    hall.save
  end
end
sectors = {
    "main_hall_sector" => {
        :name => "Главный",
        :schema_id => SchemaItem.find_by(:alias => "main_hall").id
    }
}
sectors.each do |key, value|
  if (Sector.find_by(:alias => key).nil?)
    Sector.new({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] }).save
  end
end
sector_id = Sector.find_by(:alias => "main_hall_sector").id
sector_groups = {
    "main_hall_hall" => {
        :name => "Зал",
        :sector_id => sector_id
    },
    "main_hall_balkon" => {
        :name => "Балкон",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
schema_id = SchemaItem.find_by(:alias => "main_hall").id
# Партер
group_id = SectorGroup.find_by(:alias => "main_hall_hall").id
seats = {
    "1" => [
        [1, group_id, from200_obj],
        [2, group_id, from200_obj],
        [3, group_id, from200_obj],
        [4, group_id, from200_obj],
        [5, group_id, from200_obj],
        [6, group_id, from200_obj],
        [7, group_id, from200_obj],
        [8, group_id, from200_obj],
        [9, group_id, from200_obj],
        [10, group_id, from200_obj],
        [11, group_id, from200_obj],
        [12, group_id, from200_obj],
        [13, group_id, from200_obj],
        [14, group_id, from200_obj],
        [15, group_id, from200_obj],
        [16, group_id, from200_obj],
        [17, group_id, from200_obj],
        [18, group_id, from200_obj],
        [19, group_id, from200_obj],
        [20, group_id, from200_obj],
        [21, group_id, from200_obj],
        [22, group_id, from200_obj]
    ],
    "2" => [
        [1, group_id, from300_obj],
        [2, group_id, from300_obj],
        [3, group_id, from300_obj],
        [4, group_id, from300_obj],
        [5, group_id, from300_obj],
        [6, group_id, from300_obj],
        [7, group_id, from300_obj],
        [8, group_id, from300_obj],
        [9, group_id, from300_obj],
        [10, group_id, from300_obj],
        [11, group_id, from300_obj],
        [12, group_id, from300_obj],
        [13, group_id, from300_obj],
        [14, group_id, from300_obj],
        [15, group_id, from300_obj],
        [16, group_id, from300_obj],
        [17, group_id, from300_obj],
        [18, group_id, from300_obj],
        [19, group_id, from300_obj],
        [20, group_id, from300_obj],
        [21, group_id, from300_obj],
        [22, group_id, from300_obj]
    ],
    "3" => [
        [1, group_id, from400_obj],
        [2, group_id, from400_obj],
        [3, group_id, from400_obj],
        [4, group_id, from400_obj],
        [5, group_id, from400_obj],
        [6, group_id, from400_obj],
        [7, group_id, from400_obj],
        [8, group_id, from400_obj],
        [9, group_id, from400_obj],
        [10, group_id, from400_obj],
        [11, group_id, from400_obj],
        [12, group_id, from400_obj],
        [13, group_id, from400_obj],
        [14, group_id, from400_obj],
        [15, group_id, from400_obj],
        [16, group_id, from400_obj],
        [17, group_id, from400_obj],
        [18, group_id, from400_obj],
        [19, group_id, from400_obj],
        [20, group_id, from400_obj],
        [21, group_id, from400_obj],
        [22, group_id, from400_obj]
    ],
    "4" => [
        [1, group_id, from500_obj],
        [2, group_id, from500_obj],
        [3, group_id, from500_obj],
        [4, group_id, from500_obj],
        [5, group_id, from500_obj],
        [6, group_id, from500_obj],
        [7, group_id, from500_obj],
        [8, group_id, from500_obj],
        [9, group_id, from500_obj],
        [10, group_id, from500_obj],
        [11, group_id, from500_obj],
        [12, group_id, from500_obj],
        [13, group_id, from500_obj],
        [14, group_id, from500_obj],
        [15, group_id, from500_obj],
        [16, group_id, from500_obj],
        [17, group_id, from500_obj],
        [18, group_id, from500_obj],
        [19, group_id, from500_obj],
        [20, group_id, from500_obj],
        [21, group_id, from500_obj],
        [22, group_id, from500_obj]
    ],
    "5" => [
        [1, group_id, from500_obj],
        [2, group_id, from500_obj],
        [3, group_id, from500_obj],
        [4, group_id, from500_obj],
        [5, group_id, from500_obj],
        [6, group_id, from500_obj],
        [7, group_id, from500_obj],
        [8, group_id, from500_obj],
        [9, group_id, from500_obj],
        [10, group_id, from500_obj],
        [11, group_id, from500_obj],
        [12, group_id, from500_obj],
        [13, group_id, from500_obj],
        [14, group_id, from500_obj],
        [15, group_id, from500_obj],
        [16, group_id, from500_obj],
        [17, group_id, from500_obj],
        [18, group_id, from500_obj],
        [19, group_id, from500_obj],
        [20, group_id, from500_obj],
        [21, group_id, from500_obj],
        [22, group_id, from500_obj]
    ],
    "6" => [
        [1, group_id, from500_obj],
        [2, group_id, from500_obj],
        [3, group_id, from500_obj],
        [4, group_id, from500_obj],
        [5, group_id, from500_obj],
        [6, group_id, from500_obj],
        [7, group_id, from500_obj],
        [8, group_id, from500_obj],
        [9, group_id, from500_obj],
        [10, group_id, from500_obj],
        [11, group_id, from500_obj],
        [12, group_id, from500_obj],
        [13, group_id, from500_obj],
        [14, group_id, from500_obj],
        [15, group_id, from500_obj],
        [16, group_id, from500_obj],
        [17, group_id, from500_obj],
        [18, group_id, from500_obj],
        [19, group_id, from500_obj],
        [20, group_id, from500_obj],
        [21, group_id, from500_obj],
        [22, group_id, from500_obj]
    ],
    "7" => [
        [1, group_id, from600_obj],
        [2, group_id, from600_obj],
        [3, group_id, from600_obj],
        [4, group_id, from600_obj],
        [5, group_id, from600_obj],
        [6, group_id, from600_obj]
    ],
    "8" => [
        [1, group_id, from600_obj],
        [2, group_id, from600_obj],
        [3, group_id, from600_obj],
        [4, group_id, from600_obj],
        [5, group_id, from600_obj],
        [6, group_id, from600_obj],
        [7, group_id, from600_obj],
        [8, group_id, from600_obj],
        [9, group_id, from600_obj],
        [10, group_id, from600_obj],
        [11, group_id, from600_obj],
        [12, group_id, from600_obj],
        [13, group_id, from600_obj],
        [14, group_id, from600_obj],
        [15, group_id, from600_obj],
        [16, group_id, from600_obj],
        [17, group_id, from600_obj],
        [18, group_id, from600_obj],
        [19, group_id, from600_obj],
        [20, group_id, from600_obj],
        [21, group_id, from600_obj],
        [22, group_id, from600_obj]
    ],
    "9" => [
        [1, group_id, from600_obj],
        [2, group_id, from600_obj],
        [3, group_id, from600_obj],
        [4, group_id, from600_obj],
        [5, group_id, from600_obj],
        [6, group_id, from600_obj],
        [7, group_id, from600_obj],
        [8, group_id, from600_obj],
        [9, group_id, from600_obj],
        [10, group_id, from600_obj],
        [11, group_id, from600_obj],
        [12, group_id, from600_obj],
        [13, group_id, from600_obj],
        [14, group_id, from600_obj],
        [15, group_id, from600_obj],
        [16, group_id, from600_obj],
        [17, group_id, from600_obj],
        [18, group_id, from600_obj],
        [19, group_id, from600_obj],
        [20, group_id, from600_obj],
        [21, group_id, from600_obj],
        [22, group_id, from600_obj]
    ],
    "10" => [
        [1, group_id, from700_obj],
        [2, group_id, from700_obj],
        [3, group_id, from700_obj],
        [4, group_id, from700_obj],
        [5, group_id, from700_obj],
        [6, group_id, from700_obj],
        [7, group_id, from700_obj],
        [8, group_id, from700_obj],
        [9, group_id, from700_obj],
        [10, group_id, from700_obj],
        [11, group_id, from700_obj],
        [12, group_id, from700_obj],
        [13, group_id, from700_obj],
        [14, group_id, from700_obj],
        [15, group_id, from700_obj],
        [16, group_id, from700_obj],
        [17, group_id, from700_obj],
        [18, group_id, from700_obj],
        [19, group_id, from700_obj],
        [20, group_id, from700_obj],
        [21, group_id, from700_obj],
        [22, group_id, from700_obj]
    ],
    "11" => [
        [1, group_id, from800_obj],
        [2, group_id, from800_obj],
        [3, group_id, from800_obj],
        [4, group_id, from800_obj],
        [5, group_id, from800_obj],
        [6, group_id, from800_obj],
        [7, group_id, from800_obj],
        [8, group_id, from800_obj],
        [9, group_id, from800_obj],
        [10, group_id, from800_obj],
        [11, group_id, from800_obj],
        [12, group_id, from800_obj],
        [13, group_id, from800_obj],
        [14, group_id, from800_obj],
        [15, group_id, from800_obj],
        [16, group_id, from800_obj],
        [17, group_id, from800_obj],
        [18, group_id, from800_obj],
        [19, group_id, from800_obj],
        [20, group_id, from800_obj],
        [21, group_id, from800_obj],
        [22, group_id, from800_obj]
    ],
    "12" => [
        [1, group_id, from800_obj],
        [2, group_id, from800_obj],
        [3, group_id, from800_obj],
        [4, group_id, from800_obj],
        [5, group_id, from800_obj],
        [6, group_id, from800_obj],
        [7, group_id, from800_obj],
        [8, group_id, from800_obj],
        [9, group_id, from800_obj],
        [10, group_id, from800_obj],
        [11, group_id, from800_obj],
        [12, group_id, from800_obj],
        [13, group_id, from800_obj],
        [14, group_id, from800_obj],
        [15, group_id, from800_obj],
        [16, group_id, from800_obj],
        [17, group_id, from800_obj],
        [18, group_id, from800_obj],
        [19, group_id, from800_obj],
        [20, group_id, from800_obj],
        [21, group_id, from800_obj],
        [22, group_id, from800_obj]
    ],
    "13" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
        [17, group_id, from900_obj],
        [18, group_id, from900_obj],
        [19, group_id, from900_obj],
        [20, group_id, from900_obj],
        [21, group_id, from900_obj],
        [22, group_id, from900_obj]
    ],
    "14" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
        [17, group_id, from900_obj],
        [18, group_id, from900_obj],
        [19, group_id, from900_obj],
        [20, group_id, from900_obj],
        [21, group_id, from900_obj],
        [22, group_id, from900_obj]
    ],
    "15" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
        [17, group_id, from900_obj],
        [18, group_id, from900_obj],
        [19, group_id, from900_obj],
        [20, group_id, from900_obj],
        [21, group_id, from900_obj],
        [22, group_id, from900_obj]
    ],
    "16" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj]
    ],
    "17" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj]
    ],
    "18" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
        [17, group_id, from900_obj],
        [18, group_id, from900_obj],
        [19, group_id, from900_obj],
        [20, group_id, from900_obj],
        [21, group_id, from900_obj],
        [22, group_id, from900_obj]
    ],
    "19" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj]
    ],
    "20" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj]
    ]
}
seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] }).save
    end
  end
end
#Балкон
group_id = SectorGroup.find_by(:alias => "main_hall_balkon").id
seats = {
    "1" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "2" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "3" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "4" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "5" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "6" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "7" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "8" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj],
        [21, group_id, from1400_obj],
        [22, group_id, from1400_obj]
    ]
}
seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] }).save
    end
  end
end
puts "Schema Main Hall import success"


# Импорт схемы "Главный зал расширенный"
puts "Schema Main Hall With Seats start import"
main_hall_schema = {
    "main_hall_with_seats" => {
        :name => "Главный зал",
        :schema_type_id => SchemaType.single_level.id,
        :hall_id => Hall.find_by(:alias => "main_hall_with_seats").id
    }
}
main_hall_schema.each do |key, value|
  if (SchemaItem.find_by(:alias => key).nil?)
    schema_item = SchemaItem.new({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
    schema_item.save
    hall = Hall.find_by(:alias => "main_hall_with_seats")
    hall.schema_item_id = schema_item.id
    hall.save
  end
end
sectors = {
    "main_hall_with_seats_sector" => {
        :name => "Главный",
        :schema_id => SchemaItem.find_by(:alias => "main_hall_with_seats").id
    }
}
sectors.each do |key, value|
  if (Sector.find_by(:alias => key).nil?)
    Sector.new({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] }).save
  end
end
sector_id = Sector.find_by(:alias => "main_hall_with_seats_sector").id
sector_groups = {
    "main_hall_with_seats_hall" => {
        :name => "Зал",
        :sector_id => sector_id
    },
    "main_hall_with_seats_balkon" => {
        :name => "Балкон",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
schema_id = SchemaItem.find_by(:alias => "main_hall_with_seats").id
# Партер
group_id = SectorGroup.find_by(:alias => "main_hall_with_seats_hall").id
seats = {
    "1" => [
        [1, group_id, from200_obj],
        [2, group_id, from200_obj],
        [3, group_id, from200_obj],
        [4, group_id, from200_obj],
        [5, group_id, from200_obj],
        [6, group_id, from200_obj],
        [7, group_id, from200_obj],
        [8, group_id, from200_obj],
        [9, group_id, from200_obj],
        [10, group_id, from200_obj],
        [11, group_id, from200_obj],
        [12, group_id, from200_obj],
        [13, group_id, from200_obj],
        [14, group_id, from200_obj],
        [15, group_id, from200_obj],
        [16, group_id, from200_obj],
        [17, group_id, from200_obj],
        [18, group_id, from200_obj],
        [19, group_id, from200_obj],
        [20, group_id, from200_obj],
        [21, group_id, from200_obj],
        [22, group_id, from200_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "2" => [
        [1, group_id, from300_obj],
        [2, group_id, from300_obj],
        [3, group_id, from300_obj],
        [4, group_id, from300_obj],
        [5, group_id, from300_obj],
        [6, group_id, from300_obj],
        [7, group_id, from300_obj],
        [8, group_id, from300_obj],
        [9, group_id, from300_obj],
        [10, group_id, from300_obj],
        [11, group_id, from300_obj],
        [12, group_id, from300_obj],
        [13, group_id, from300_obj],
        [14, group_id, from300_obj],
        [15, group_id, from300_obj],
        [16, group_id, from300_obj],
        [17, group_id, from300_obj],
        [18, group_id, from300_obj],
        [19, group_id, from300_obj],
        [20, group_id, from300_obj],
        [21, group_id, from300_obj],
        [22, group_id, from300_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "3" => [
        [1, group_id, from400_obj],
        [2, group_id, from400_obj],
        [3, group_id, from400_obj],
        [4, group_id, from400_obj],
        [5, group_id, from400_obj],
        [6, group_id, from400_obj],
        [7, group_id, from400_obj],
        [8, group_id, from400_obj],
        [9, group_id, from400_obj],
        [10, group_id, from400_obj],
        [11, group_id, from400_obj],
        [12, group_id, from400_obj],
        [13, group_id, from400_obj],
        [14, group_id, from400_obj],
        [15, group_id, from400_obj],
        [16, group_id, from400_obj],
        [17, group_id, from400_obj],
        [18, group_id, from400_obj],
        [19, group_id, from400_obj],
        [20, group_id, from400_obj],
        [21, group_id, from400_obj],
        [22, group_id, from400_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "4" => [
        [1, group_id, from500_obj],
        [2, group_id, from500_obj],
        [3, group_id, from500_obj],
        [4, group_id, from500_obj],
        [5, group_id, from500_obj],
        [6, group_id, from500_obj],
        [7, group_id, from500_obj],
        [8, group_id, from500_obj],
        [9, group_id, from500_obj],
        [10, group_id, from500_obj],
        [11, group_id, from500_obj],
        [12, group_id, from500_obj],
        [13, group_id, from500_obj],
        [14, group_id, from500_obj],
        [15, group_id, from500_obj],
        [16, group_id, from500_obj],
        [17, group_id, from500_obj],
        [18, group_id, from500_obj],
        [19, group_id, from500_obj],
        [20, group_id, from500_obj],
        [21, group_id, from500_obj],
        [22, group_id, from500_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "5" => [
        [1, group_id, from500_obj],
        [2, group_id, from500_obj],
        [3, group_id, from500_obj],
        [4, group_id, from500_obj],
        [5, group_id, from500_obj],
        [6, group_id, from500_obj],
        [7, group_id, from500_obj],
        [8, group_id, from500_obj],
        [9, group_id, from500_obj],
        [10, group_id, from500_obj],
        [11, group_id, from500_obj],
        [12, group_id, from500_obj],
        [13, group_id, from500_obj],
        [14, group_id, from500_obj],
        [15, group_id, from500_obj],
        [16, group_id, from500_obj],
        [17, group_id, from500_obj],
        [18, group_id, from500_obj],
        [19, group_id, from500_obj],
        [20, group_id, from500_obj],
        [21, group_id, from500_obj],
        [22, group_id, from500_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "6" => [
        [1, group_id, from500_obj],
        [2, group_id, from500_obj],
        [3, group_id, from500_obj],
        [4, group_id, from500_obj],
        [5, group_id, from500_obj],
        [6, group_id, from500_obj],
        [7, group_id, from500_obj],
        [8, group_id, from500_obj],
        [9, group_id, from500_obj],
        [10, group_id, from500_obj],
        [11, group_id, from500_obj],
        [12, group_id, from500_obj],
        [13, group_id, from500_obj],
        [14, group_id, from500_obj],
        [15, group_id, from500_obj],
        [16, group_id, from500_obj],
        [17, group_id, from500_obj],
        [18, group_id, from500_obj],
        [19, group_id, from500_obj],
        [20, group_id, from500_obj],
        [21, group_id, from500_obj],
        [22, group_id, from500_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "7" => [
        [1, group_id, from600_obj],
        [2, group_id, from600_obj],
        [3, group_id, from600_obj],
        [4, group_id, from600_obj],
        [5, group_id, from600_obj],
        [6, group_id, from600_obj]
    ],
    "8" => [
        [1, group_id, from600_obj],
        [2, group_id, from600_obj],
        [3, group_id, from600_obj],
        [4, group_id, from600_obj],
        [5, group_id, from600_obj],
        [6, group_id, from600_obj],
        [17, group_id, from600_obj],
        [18, group_id, from600_obj],
        [19, group_id, from600_obj],
        [20, group_id, from600_obj],
        [21, group_id, from600_obj],
        [22, group_id, from600_obj]
    ],
    "9" => [
        [1, group_id, from600_obj],
        [2, group_id, from600_obj],
        [3, group_id, from600_obj],
        [4, group_id, from600_obj],
        [5, group_id, from600_obj],
        [6, group_id, from600_obj],
        [7, group_id, from600_obj],
        [8, group_id, from600_obj],
        [9, group_id, from600_obj],
        [10, group_id, from600_obj],
        [11, group_id, from600_obj],
        [12, group_id, from600_obj],
        [13, group_id, from600_obj],
        [14, group_id, from600_obj],
        [15, group_id, from600_obj],
        [16, group_id, from600_obj],
        [17, group_id, from600_obj],
        [18, group_id, from600_obj],
        [19, group_id, from600_obj],
        [20, group_id, from600_obj],
        [21, group_id, from600_obj],
        [22, group_id, from600_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "10" => [
        [1, group_id, from700_obj],
        [2, group_id, from700_obj],
        [3, group_id, from700_obj],
        [4, group_id, from700_obj],
        [5, group_id, from700_obj],
        [6, group_id, from700_obj],
        [7, group_id, from700_obj],
        [8, group_id, from700_obj],
        [9, group_id, from700_obj],
        [10, group_id, from700_obj],
        [11, group_id, from700_obj],
        [12, group_id, from700_obj],
        [13, group_id, from700_obj],
        [14, group_id, from700_obj],
        [15, group_id, from700_obj],
        [16, group_id, from700_obj],
        [17, group_id, from700_obj],
        [18, group_id, from700_obj],
        [19, group_id, from700_obj],
        [20, group_id, from700_obj],
        [21, group_id, from700_obj],
        [22, group_id, from700_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "11" => [
        [1, group_id, from800_obj],
        [2, group_id, from800_obj],
        [3, group_id, from800_obj],
        [4, group_id, from800_obj],
        [5, group_id, from800_obj],
        [6, group_id, from800_obj],
        [7, group_id, from800_obj],
        [8, group_id, from800_obj],
        [9, group_id, from800_obj],
        [10, group_id, from800_obj],
        [11, group_id, from800_obj],
        [12, group_id, from800_obj],
        [13, group_id, from800_obj],
        [14, group_id, from800_obj],
        [15, group_id, from800_obj],
        [16, group_id, from800_obj],
        [17, group_id, from800_obj],
        [18, group_id, from800_obj],
        [19, group_id, from800_obj],
        [20, group_id, from800_obj],
        [21, group_id, from800_obj],
        [22, group_id, from800_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "12" => [
        [1, group_id, from800_obj],
        [2, group_id, from800_obj],
        [3, group_id, from800_obj],
        [4, group_id, from800_obj],
        [5, group_id, from800_obj],
        [6, group_id, from800_obj],
        [7, group_id, from800_obj],
        [8, group_id, from800_obj],
        [9, group_id, from800_obj],
        [10, group_id, from800_obj],
        [11, group_id, from800_obj],
        [12, group_id, from800_obj],
        [13, group_id, from800_obj],
        [14, group_id, from800_obj],
        [15, group_id, from800_obj],
        [16, group_id, from800_obj],
        [17, group_id, from800_obj],
        [18, group_id, from800_obj],
        [19, group_id, from800_obj],
        [20, group_id, from800_obj],
        [21, group_id, from800_obj],
        [22, group_id, from800_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "13" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
        [17, group_id, from900_obj],
        [18, group_id, from900_obj],
        [19, group_id, from900_obj],
        [20, group_id, from900_obj],
        [21, group_id, from900_obj],
        [22, group_id, from900_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "14" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
        [17, group_id, from900_obj],
        [18, group_id, from900_obj],
        [19, group_id, from900_obj],
        [20, group_id, from900_obj],
        [21, group_id, from900_obj],
        [22, group_id, from900_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "15" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
        [17, group_id, from900_obj],
        [18, group_id, from900_obj],
        [19, group_id, from900_obj],
        [20, group_id, from900_obj],
        [21, group_id, from900_obj],
        [22, group_id, from900_obj],
        # Specials
        [23, group_id, from200_obj, "7А"],
        [24, group_id, from200_obj, "16А"]
    ],
    "16" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
    ],
    "17" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj]
    ],
    "18" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj],
        [13, group_id, from900_obj],
        [14, group_id, from900_obj],
        [15, group_id, from900_obj],
        [16, group_id, from900_obj],
        [17, group_id, from900_obj],
        [18, group_id, from900_obj],
        [19, group_id, from900_obj],
        [20, group_id, from900_obj],
        [21, group_id, from900_obj],
        [22, group_id, from900_obj]
    ],
    "19" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        # Specials
        [8, group_id, from200_obj, "1А"],
        [9, group_id, from200_obj, "1Б"]
    ],
    "20" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        # Specials
        [8, group_id, from200_obj, "1А"],
        [9, group_id, from200_obj, "1Б"]
    ]
}
seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      item = SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] })
      if !seat[3].nil?
        item.special = seat[3]
      end
      item.save
    end
  end
end
#Балкон
group_id = SectorGroup.find_by(:alias => "main_hall_with_seats_balkon").id
seats = {
    "1" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "2" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "3" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "4" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "5" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "6" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "7" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj]
    ],
    "8" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
        [10, group_id, from1400_obj],
        [11, group_id, from1400_obj],
        [12, group_id, from1400_obj],
        [13, group_id, from1400_obj],
        [14, group_id, from1400_obj],
        [15, group_id, from1400_obj],
        [16, group_id, from1400_obj],
        [17, group_id, from1400_obj],
        [18, group_id, from1400_obj],
        [19, group_id, from1400_obj],
        [20, group_id, from1400_obj],
        [21, group_id, from1400_obj],
        [22, group_id, from1400_obj]
    ],
}
seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] }).save
    end
  end
end
puts "Schema Main Hall With Seats import success"

# Импорт схемы "Главный зал расширенный"
puts "Schema Theatr Hall start import"
theatr_hall_schema = {
    "theatr_hall" => {
        :name => "Театральный зал",
        :schema_type_id => SchemaType.single_level.id,
        :hall_id => Hall.find_by(:alias => "theatr_hall").id
    }
}
theatr_hall_schema.each do |key, value|
  if (SchemaItem.find_by(:alias => key).nil?)
    schema_item = SchemaItem.new({ :alias => key , :name => value[:name], :schema_type_id => value[:schema_type_id], :hall_id => value[:hall_id] })
    schema_item.save
    hall = Hall.find_by(:alias => "theatr_hall")
    hall.schema_item_id = schema_item.id
    hall.save
  end
end
sectors = {
    "theatr_hall_sector" => {
        :name => "Главный",
        :schema_id => SchemaItem.find_by(:alias => "theatr_hall").id
    }
}
sectors.each do |key, value|
  if (Sector.find_by(:alias => key).nil?)
    Sector.new({ :alias => key , :name => value[:name], :schema_item_id => value[:schema_id] }).save
  end
end
sector_id = Sector.find_by(:alias => "theatr_hall_sector").id
sector_groups = {
    "theatr_hall_hall" => {
        :name => "Зал",
        :sector_id => sector_id
    }
}
sector_groups.each do |key, value|
  if (SectorGroup.find_by(:alias => key).nil?)
    SectorGroup.new({ :alias => key , :name => value[:name], :sector_id => value[:sector_id] }).save
  end
end
schema_id = SchemaItem.find_by(:alias => "theatr_hall").id
# Партер
group_id = SectorGroup.find_by(:alias => "theatr_hall_hall").id
seats = {
    "0" => [
        [1, group_id, from200_obj],
        [2, group_id, from200_obj],
        [3, group_id, from200_obj],
        [4, group_id, from200_obj],
        [5, group_id, from200_obj],
        [6, group_id, from200_obj],
        [7, group_id, from200_obj],
        [8, group_id, from200_obj],
        [9, group_id, from200_obj],
        [10, group_id, from200_obj],
        [11, group_id, from200_obj],
        [12, group_id, from200_obj]
    ],
    "1" => [
        [1, group_id, from200_obj],
        [2, group_id, from200_obj],
        [3, group_id, from200_obj],
        [4, group_id, from200_obj],
        [5, group_id, from200_obj],
        [6, group_id, from200_obj],
        [7, group_id, from200_obj],
        [8, group_id, from200_obj],
        [9, group_id, from200_obj],
        [10, group_id, from200_obj],
        [11, group_id, from200_obj],
        [12, group_id, from200_obj]
    ],
    "2" => [
        [1, group_id, from300_obj],
        [2, group_id, from300_obj],
        [3, group_id, from300_obj],
        [4, group_id, from300_obj],
        [5, group_id, from300_obj],
        [6, group_id, from300_obj],
        [7, group_id, from300_obj],
        [8, group_id, from300_obj],
        [9, group_id, from300_obj],
        [10, group_id, from300_obj],
        [11, group_id, from300_obj],
        [12, group_id, from300_obj]
    ],
    "3" => [
        [1, group_id, from400_obj],
        [2, group_id, from400_obj],
        [3, group_id, from400_obj],
        [4, group_id, from400_obj],
        [5, group_id, from400_obj],
        [6, group_id, from400_obj],
        [7, group_id, from400_obj],
        [8, group_id, from400_obj],
        [9, group_id, from400_obj],
        [10, group_id, from400_obj],
        [11, group_id, from400_obj],
        [12, group_id, from400_obj]
    ],
    "4" => [
        [1, group_id, from500_obj],
        [2, group_id, from500_obj],
        [3, group_id, from500_obj],
        [4, group_id, from500_obj],
        [5, group_id, from500_obj],
        [6, group_id, from500_obj],
        [7, group_id, from500_obj],
        [8, group_id, from500_obj],
        [9, group_id, from500_obj],
        [10, group_id, from500_obj],
        [11, group_id, from500_obj],
        [12, group_id, from500_obj]
    ],
    "5" => [
        [1, group_id, from600_obj],
        [2, group_id, from600_obj],
        [3, group_id, from600_obj],
        [4, group_id, from600_obj],
        [5, group_id, from600_obj],
        [6, group_id, from600_obj],
        [7, group_id, from600_obj],
        [8, group_id, from600_obj],
        [9, group_id, from600_obj],
        [10, group_id, from600_obj],
        [11, group_id, from600_obj],
        [12, group_id, from600_obj]
    ],
    "6" => [
        [1, group_id, from700_obj],
        [2, group_id, from700_obj],
        [3, group_id, from700_obj],
        [4, group_id, from700_obj],
        [5, group_id, from700_obj],
        [6, group_id, from700_obj],
        [7, group_id, from700_obj],
        [8, group_id, from700_obj],
        [9, group_id, from700_obj],
        [10, group_id, from700_obj],
        [11, group_id, from700_obj],
        [12, group_id, from700_obj]
    ],
    "7" => [
        [1, group_id, from800_obj],
        [2, group_id, from800_obj],
        [3, group_id, from800_obj],
        [4, group_id, from800_obj],
        [5, group_id, from800_obj],
        [6, group_id, from800_obj],
        [7, group_id, from800_obj],
        [8, group_id, from800_obj],
        [9, group_id, from800_obj],
        [10, group_id, from800_obj],
        [11, group_id, from800_obj],
        [12, group_id, from800_obj]
    ],
    "8" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj]
    ],
    "9" => [
        [1, group_id, from900_obj],
        [2, group_id, from900_obj],
        [3, group_id, from900_obj],
        [4, group_id, from900_obj],
        [5, group_id, from900_obj],
        [6, group_id, from900_obj],
        [7, group_id, from900_obj],
        [8, group_id, from900_obj],
        [9, group_id, from900_obj],
        [10, group_id, from900_obj],
        [11, group_id, from900_obj],
        [12, group_id, from900_obj]
    ],
    "10" => [
        [1, group_id, from1400_obj],
        [2, group_id, from1400_obj],
        [3, group_id, from1400_obj],
        [4, group_id, from1400_obj],
        [5, group_id, from1400_obj],
        [6, group_id, from1400_obj],
        [7, group_id, from1400_obj],
        [8, group_id, from1400_obj],
        [9, group_id, from1400_obj],
    ],
}
seats.each do |key, value|
  value.each do |seat|
    if SeatItem.find_by(:row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s).nil?
      item = SeatItem.new({ :row => key, :number => seat[0].to_s, :sector_group_id => seat[1].to_s, :schema_item_id => schema_id, :seat_item_type_id => seat[2] })
      if !seat[3].nil?
        item.special = seat[3]
      end
      item.save
    end
  end
end
puts "Schema Theatr Hall import success"


# generate tickets for new seats

events = EventItem.all

events.each do | event |
    hall_id = event.hall_id
    schema_object = SchemaItem.find_by(:hall_id => hall_id)
    seats_ids_by_event_tickets = []
    
    event.tickets.each do |ticket|
        seats_ids_by_event_tickets << ticket.seat_item.id
    end
    seats_ids_by_schema = SeatItem.where(:schema_item_id => schema_object.id).map {|schema_seat| schema_seat.id}

    if seats_ids_by_schema.count > seats_ids_by_event_tickets.count
        arr = []
        seats_ids_without_tickets = seats_ids_by_schema - seats_ids_by_event_tickets

        seats_ids_without_tickets.each do | seat_id |
            seat = SeatItem.find(seat_id)
            ticket_state_id = TicketState.find_by(:alias => TicketState.AVAILABLE_ALIAS).id
            arr << {
                :seat_item_id    => seat.id, 
                :price           => seat.seat_item_type.price, 
                :event_item_id   => event.id, 
                :ticket_state_id => ticket_state_id 
            }
        end
        Ticket.create(arr)
    end

end

puts "Tickets for new seats was generated successfuly"