class RemoveUserAndSystemUserFromTicket < ActiveRecord::Migration

  def up
    remove_column :tickets, :user_id
  end

  def down
    add_column :tickets, :user_id, :integer
  end

end
