class ChangeSeatColumnInTicket < ActiveRecord::Migration
  def up
    remove_column :tickets, :seat_id
    add_column :tickets, :seat_item_id, :integer
  end

  def down
    add_column :tickets, :seat_id, :integer
    remove_column :tickets, :seat_item_id
  end

end
