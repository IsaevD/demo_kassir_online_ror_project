class CreateModulesDModuleItems < ActiveRecord::Migration
  def change
    create_table :modules_d_module_items do |t|
      t.string :alias
      t.string :module_name
      t.string :name
      t.text :description
      t.string :git_url
      t.timestamps
    end
  end
end
