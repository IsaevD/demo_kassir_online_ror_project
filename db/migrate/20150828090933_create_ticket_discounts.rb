class CreateTicketDiscounts < ActiveRecord::Migration
  def change
    create_table :ticket_discounts do |t|
      t.string :alias
      t.string :name
      t.integer :value
      t.boolean :is_percent
      t.integer :system_state_id
      t.timestamps
    end
  end
end
