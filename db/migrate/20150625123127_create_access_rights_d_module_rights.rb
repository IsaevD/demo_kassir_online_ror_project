class CreateAccessRightsDModuleRights < ActiveRecord::Migration
  def change
    create_table :access_rights_d_module_rights do |t|
      t.integer :access_right_id
      t.integer :module_item_id
      t.timestamps
    end
  end
end
