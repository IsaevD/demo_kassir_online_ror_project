class CreateSeatItems < ActiveRecord::Migration
  def change
    create_table :seat_items do |t|
      t.string :row
      t.string :number
      t.integer :sector_group_id
      t.timestamps
    end
  end
end
