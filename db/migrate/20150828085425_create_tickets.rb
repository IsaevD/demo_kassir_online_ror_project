class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :seat_id
      t.integer :price
      t.integer :ticket_state_id
      t.integer :event_item_id
      t.integer :user_id
      t.timestamps
    end
  end
end
