class AddDeliveryMethodToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :phone, :string
    add_column :payments, :delivery_address, :string
    add_column :payments, :delivery_method_id, :integer
  end
end
