class AddEventItemImage < ActiveRecord::Migration
  def up
    add_attachment :event_items, :image
  end

  def down
    remove_attachment :event_items, :image
  end

end
