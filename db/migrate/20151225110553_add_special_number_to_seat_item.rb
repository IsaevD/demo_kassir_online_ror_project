class AddSpecialNumberToSeatItem < ActiveRecord::Migration
  def change
    add_column :seat_items, :special, :string
  end
end
