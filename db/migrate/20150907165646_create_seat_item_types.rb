class CreateSeatItemTypes < ActiveRecord::Migration
  def change
    create_table :seat_item_types do |t|
      t.string :alias
      t.string :name
      t.string :color
      t.integer :price
      t.timestamps
    end
  end
end
