class AddAvatarFieldToSystemUser < ActiveRecord::Migration
  def up
    add_attachment :system_main_items_d_system_users, :avatar
  end

  def down
    remove_attachment :system_main_items_d_system_users, :avatar
  end
end
