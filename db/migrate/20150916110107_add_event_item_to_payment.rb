class AddEventItemToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :event_item_id, :integer
  end
end
